#ifndef __ENGINE_THERMAL_EQUIVALENTCIRCUIT_LINEARCIRCUIT__
#define __ENGINE_THERMAL_EQUIVALENTCIRCUIT_LINEARCIRCUIT__

#include <util/math/vector.h>
#include <util/math/matrix.h>

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
			class linear_circuit {
				private:
					util::math::matrix<double> A;
					util::math::vector<double> bi;
					util::math::matrix<double> bc;
					util::math::vector<double> v;

					util::math::matrix<double> lua;
					bool lua_corrupt;

				public:
					linear_circuit(unsigned nodes);

					util::math::matrix<double> &get_A() {return A;}
					util::math::vector<double> &get_bi() {return bi;}
					util::math::matrix<double> &get_bc() {return bc;}
					util::math::vector<double> &get_v() {return v;}

					const util::math::vector<double> &get_v() const {return v;}

					void solve();

					void mark_lua_corrupt() {
						lua_corrupt = true;
					}
			};
		}
	}
}

#endif // __ENGINE_THERMAL_EQUIVALENTCIRCUIT_LINEARCIRCUIT__
