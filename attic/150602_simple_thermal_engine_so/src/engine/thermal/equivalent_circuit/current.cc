#include <util/math/vector.h>
#include <engine/thermal/equivalent_circuit/current.h>
#include <engine/thermal/equivalent_circuit/linear_circuit.h>

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
			void current::add_to(linear_circuit &circuit) {
				util::math::vector<double> &bi = circuit.get_bi();

				if (node1 != 0) bi[node1 - 1] += value;
				if (node2 != 0) bi[node2 - 1] -= value;
			}

			void current::remove_from(linear_circuit &circuit) {
				util::math::vector<double> &bi = circuit.get_bi();

				if (node1 != 0) bi[node1 - 1] -= value;
				if (node2 != 0) bi[node2 - 1] += value;
			}

			void current::set_value(double value) {
				this->value = value;
			}
		}
	}
}

