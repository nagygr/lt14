#include <engine/thermal/equivalent_circuit/linear_circuit.h>

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
			linear_circuit::linear_circuit(unsigned nodes) : A(nodes - 1, nodes - 1), bi(nodes - 1), bc(nodes - 1, nodes - 1), v(nodes - 1), lua(nodes - 1, nodes - 1), lua_corrupt(true) {
				for (unsigned i = 0;  i < nodes - 1; ++i) {
					bi[i] = 0.0;
					v[i] = 0.0;

					for (unsigned j = 0; j < nodes - 1; ++j) {
						A(i,j) = 0.0;
						lua(i, j) = 0.0;
						bc(i,j) = 0.0;
					}
				}
			}

			void linear_circuit::solve() {
				util::math::vector<double> b = bc * v + bi;

				if (lua_corrupt) {
					lua = A.get_lu();
					lua_corrupt = false;
				}

				v = util::math::matrix<double>::solve(lua, b);
			}
		}
	}
}
