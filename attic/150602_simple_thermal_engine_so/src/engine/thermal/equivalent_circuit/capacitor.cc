#include <util/math/matrix.h>
#include <engine/thermal/equivalent_circuit/capacitor.h>
#include <engine/thermal/equivalent_circuit/linear_circuit.h>

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
			void capacitor::add_to(linear_circuit &circuit) {
				circuit.mark_lua_corrupt();
				util::math::matrix<double> &A = circuit.get_A();
				util::math::matrix<double> &bc = circuit.get_bc();

				if (node1 != 0) {
					A(node1 - 1, node1 - 1) += value;
					bc(node1 - 1, node1 - 1) += value;

					if (node2 != 0) {
						A(node1 - 1, node2 - 1) -= value;
						bc(node1 - 1, node2 - 1) -= value;

						A(node2 - 1, node1 - 1) -= value;
						bc(node2 - 1, node1 - 1) -= value;
					}
				}

				if (node2 != 0) {
					A(node2 - 1, node2 - 1) += value;
					bc(node2 - 1, node2 - 1) += value;
				}
			}

			void capacitor::remove_from(linear_circuit &circuit) {
				circuit.mark_lua_corrupt();
				util::math::matrix<double> &A = circuit.get_A();
				util::math::matrix<double> &bc = circuit.get_bc();

				if (node1 != 0) {
					A(node1 - 1, node1 - 1) -= value;
					bc(node1 - 1, node1 - 1) -= value;

					if (node2 != 0) {
						A(node1 - 1, node2 - 1) += value;
						bc(node1 - 1, node2 - 1) += value;

						A(node2 - 1, node1 - 1) += value;
						bc(node2 - 1, node1 - 1) += value;
					}
				}

				if (node2 != 0) {
					A(node2 - 1, node2 - 1) -= value;
					bc(node2 - 1, node2 - 1) -= value;
				}
			}

			void capacitor::set_value(double capacitance) {
				this->value = capacitance / timestep;
			}
		}
	}
}
