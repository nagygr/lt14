#ifndef __ENGINE_THERMAL_EQUIVALENTCIRCUIT_CAPACITOR__
#define __ENGINE_THERMAL_EQUIVALENTCIRCUIT_CAPACITOR__

#include <engine/thermal/equivalent_circuit/element.h>

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
			class capacitor : public element {
				protected:
					double timestep;

				public:
					capacitor(unsigned node1, unsigned node2, double capacitance, double timestep) : element(node1, node2, capacitance / timestep), timestep(timestep) {}
					virtual void add_to(linear_circuit &circuit) override;
					virtual void remove_from(linear_circuit &circuit) override;
					virtual void set_value(double capacitance) override;
			};
		}
	}
}


#endif //__ENGINE_THERMAL_EQUIVALENTCIRCUIT_CAPACITOR__
