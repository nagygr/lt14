#include <util/math/matrix.h>
#include <engine/thermal/equivalent_circuit/resistor.h>
#include <engine/thermal/equivalent_circuit/linear_circuit.h>

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
			void resistor::add_to(linear_circuit &circuit) {
				circuit.mark_lua_corrupt();
				util::math::matrix<double> &A = circuit.get_A();

				if (node1 != 0) {
					A(node1 - 1, node1 - 1) += value;

					if (node2 != 0) {
						A(node1 - 1, node2 - 1) -= value;
						A(node2 - 1, node1 - 1) -= value;
					}
				}

				if (node2 != 0) {
					A(node2 - 1, node2 - 1) += value;
				}
			}

			void resistor::remove_from(linear_circuit &circuit) {
				circuit.mark_lua_corrupt();
				util::math::matrix<double> &A = circuit.get_A();

				if (node1 != 0) {
					A(node1 - 1, node1 - 1) -= value;

					if (node2 != 0) {
						A(node1 - 1, node2 - 1) += value;
						A(node2 - 1, node1 - 1) += value;
					}
				}

				if (node2 != 0) {
					A(node2 - 1, node2 - 1) -= value;
				}
			}

			void resistor::set_value(double resistance) {
				this->value = 1.0 / resistance;
			}
		}
	}
}
