#include <util/math/matrix.h>

template <>
std::ostream &operator <<(std::ostream &os, const util::math::matrix<signed char> &m) {
	for (unsigned i = 0; i < m.get_rows(); ++i) {
		for (unsigned j = 0; j < m.get_cols(); ++j) {
			os << ((int)m(i, j)) << " ";
		}
		os << std::endl;
	}

	return os;
}

