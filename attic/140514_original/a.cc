#include <map>
#include <vector>
#include <string>
#include <memory>
#include <sstream>
#include <fstream>
#include <utility>
#include <iostream>
#include <algorithm>
#include <functional>
#include <unordered_map>

using time_type = unsigned;

class port {
	private:
		time_type time_;

	public:
		virtual void set_time(time_type const &now) {time_ = now;}
		virtual void at_simulation_cycle_start() = 0;
		virtual void at_simulation_cycle_end() = 0;
		virtual ~port() {}

		time_type const &get_time() const {return time_;}
};

template <typename data_type>
class input_port : public port {
	private:
		time_type time_;
	public:
		virtual void write(data_type const &value) = 0;
		virtual data_type const &read() const = 0;
};

template <typename data_type>
class sensitive_input_port : public input_port<data_type> {
	private:
		data_type value_;
		std::function<void()> operation_;

	public:
		sensitive_input_port(std::function<void()> operation = nullptr) : value_(), operation_(operation) {}
		virtual void write(data_type const &value) override;
		virtual data_type const &read() const override {return value_;}
		virtual void at_simulation_cycle_start() override {}
		virtual void at_simulation_cycle_end() override {}
};

template <typename data_type>
void sensitive_input_port<data_type>::write(data_type const &value) {
	if (value_ != value) {
		value_ = value;
		if (operation_) operation_();
	}
}

template <typename data_type>
class delta_input_port : public input_port<data_type> {
	private:
		data_type current_value_;
		data_type new_value_;

	public:
		delta_input_port() : current_value_(), new_value_() {}
		virtual void write(data_type const &value) override {new_value_ = value;}
		virtual data_type const &read() const override {return current_value_;}
		virtual void at_simulation_cycle_start() override {}
		virtual void at_simulation_cycle_end() {current_value_ = new_value_;}

	protected:
		data_type const &get_new_value() const {return new_value_;}
};

template <typename data_type>
class output_port : public port {
	private:
		data_type value_;
		std::vector<input_port<data_type> *> connections_;

		time_type delay_;
		bool next_value_set_;
		std::pair<time_type, data_type> next_value_;

	public:
		output_port(time_type const &delay = time_type(0)) : value_(), delay_(delay), next_value_set_(false) {}
		void write(data_type const &value);
		data_type const &read() const {return value_;}
		void connect(input_port<data_type> &port) {connections_.push_back(&port);}
		time_type &delay() {return delay_;}
		virtual void at_simulation_cycle_start() override;
		virtual void at_simulation_cycle_end() override {}
};

template <typename data_type>
void output_port<data_type>::write(data_type const &value) {
	if (delay_ == time_type(0)) {
		value_ = value;
		for (auto c: connections_) {
			c->write(value_);
		}
	}
	else {
		next_value_set_ = true;
		next_value_ = std::make_pair(get_time() + delay_, value);
	}
}

template <typename data_type>
void output_port<data_type>::at_simulation_cycle_start() {
	if (next_value_set_ && get_time() == next_value_.first) {
		value_ = next_value_.second;
		next_value_set_ = false;
		for (auto c: connections_) {
			c->write(value_);
		}
	}
}

template <typename data_type>
using signal = output_port<data_type>;

std::vector<port *> &operator <<(std::vector<port *> &ports, port &a_port) {
	ports.push_back(&a_port);
	return ports;
}

class component {
	private:
		static std::vector<component *> components_;

	private:
		std::string name_;
		std::vector<port *> ports_;

	public:
		component(std::string const &name) : name_(name) {
			components_.push_back(this);	
		}

		virtual ~component() {
			components_.erase(std::find(components_.begin(), components_.end(), this));	
		}

		std::string const &get_name() const {return name_;}

		static void set_time(time_type const &now);

		static void start_simulation_cycle();
		static void end_simulation_cycle();

	protected:
		std::vector<port *> &ports() {return ports_;}
};

std::vector<component *> component::components_;

void component::set_time(time_type const &now) {
	for (auto c: components_) {
		for (auto p: c->ports()) p->set_time(now);
	}
}

void component::start_simulation_cycle() {
	for (auto c: components_) {
		for (auto p: c->ports()) p->at_simulation_cycle_start();
	}
}

void component::end_simulation_cycle() {
	for (auto c: components_) {
		for (auto p: c->ports()) p->at_simulation_cycle_end();
	}
}

class counter : public component {
	private:
		unsigned value_;

	public:
		sensitive_input_port<bool> clk;
		delta_input_port<bool> reset;
		output_port<unsigned> output;

	public:
		counter(std::string const &name) : component(name), clk(std::bind(&counter::operation, this)), output(1) {
			ports() << clk << reset << output;
		}

		void operation() {
			if (reset.read()) {
				value_ = 0;
			}
			else if (clk.read()) {
				++value_;
			}

			output.write(value_);
		}
};

using event_log = std::map<time_type, std::vector<std::pair<std::string, std::string>>>;//< Stores events: time points are mapped to a vector of pairs of component names and values (as strings)

class logger : public component {
	private:
		event_log &events_;

	public:
		logger(std::string const &name, event_log &events) : component(name), events_(events) {}
		event_log &events() {return events_;}
		virtual std::string get_vcd_type_size() const = 0;
		virtual ~logger() {}
};

template <typename data_type>
std::string vcd_type_size() {return "real 32";}

template <>
std::string vcd_type_size<bool>() {return "wire 1";}

template <typename data_type>
class typed_logger : public logger {
	public:
		sensitive_input_port<data_type> input;

	public:
		typed_logger(std::string const &name, event_log &events) : logger(name, events), input(std::bind(&typed_logger<data_type>::operation, this)) {
			ports() << input;
		}

		void operation() {
			std::stringstream value;
			if (vcd_type_size<data_type>().find("real") == 0) value << "r"; else value << "b";
			value << input.read();
			events()[input.get_time()].push_back(std::make_pair(get_name(), value.str()));
		}

		virtual std::string get_vcd_type_size() const override {
			return vcd_type_size<data_type>();
		}
};

class event_logger {
	private:
		event_log events_; 
		std::vector<std::shared_ptr<logger>> loggers_;

	public:
		template <typename data_type>
		sensitive_input_port<data_type> &log_as(std::string const &name) {
			auto new_logger = std::make_shared<typed_logger<data_type>>(name, events_);
			loggers_.push_back(new_logger);
			return new_logger->input;
		}

		virtual void create_log() = 0;
		virtual ~event_logger() {}

	protected:
		event_log &events() {return events_;}
		std::vector<std::shared_ptr<logger>> &loggers() {return loggers_;}
};

class ostream_event_logger : public event_logger {
	private:
		std::ostream &os_;

	public:
		ostream_event_logger(std::ostream &os) : os_(os) {}

		virtual void create_log() override {
			for (auto e: events()) {
				os_ << "<<< " << e.first << " >>>" << std::endl;
				for (auto name_value: e.second) {
					os_ << "\t" << name_value.first << ": " << name_value.second << std::endl;
				}
			}
		}
};

/**
 * @todo time scale using time_type
 */
class vcd_event_logger : public event_logger {
	private:
		std::string file_name_;

	public:
		vcd_event_logger(std::string const &file_name) : file_name_(file_name) {}

		virtual void create_log() override;
};

void vcd_event_logger::create_log() {
	std::unordered_map<std::string, std::pair<char, std::string>> variables;

	for (auto l: loggers()) {
		variables[l->get_name()] = std::pair<char, std::string>('!' + variables.size(), l->get_vcd_type_size());
	}

	std::fstream vcd(file_name_, std::ios::out);

	vcd << "$timescale 1us $end" << std::endl;

	for (auto v: variables) {
		vcd << "$var " << v.second.second << " " << v.second.first << " " << v.first << " $end" << std::endl;
	}

	for (auto e: events()) {
		vcd << "#" << e.first << std::endl;
		for (auto name_value: e.second) {
			vcd << name_value.second << " " << variables[name_value.first].first << std::endl;
		}
	}
}

class simulation {
	public:
		class test_injector {
			private:
				std::unordered_map<time_type, std::vector<std::function<void()>>> &test_vector_;
				time_type index_;

			public:
				test_injector(std::unordered_map<time_type, std::vector<std::function<void()>>> &test_vector, time_type index) :
					test_vector_(test_vector),
					index_(index) {}

				test_injector &operator<<(std::function<void()> a_function) {
					test_vector_[index_].push_back(a_function);
					return *this;
				}
		};

	private:
		std::unordered_map<time_type, std::vector<std::function<void()>>> test_vector_;

	public:
		test_injector operator[](time_type index) {
			return test_injector(test_vector_, index);
		}

		void run(time_type const &start, time_type const &end, time_type const &step) {
			for (time_type now = start; now != end; now += step) {
				component::set_time(now);
				component::start_simulation_cycle();

				auto tests = test_vector_.find(now);
				if (tests != test_vector_.end()) {
					for (auto f: tests->second) {
						f();
					}
				}

				component::end_simulation_cycle();
			}
		}
};

void test1() {
	signal<bool> clock;
	signal<bool> reset;

	counter cnt("Counter");

	//ostream_event_logger logger(std::cout);
	vcd_event_logger logger("test.vcd");

	clock.connect(cnt.clk);
	reset.connect(cnt.reset);

	clock.connect(logger.log_as<bool>("clock"));
	reset.connect(logger.log_as<bool>("reset"));
	cnt.output.connect(logger.log_as<unsigned>("counter"));

	simulation sim;

	sim[0]	<< [&]{reset.write(0);} 
	<< [&]{clock.write(0);};

	sim[1]	<< [&]{reset.write(1);}
	<< [&]{clock.write(0);};

	sim[2]	<< [&]{clock.write(1);};

	sim[3]	<< [&]{reset.write(0);}
	<< [&]{clock.write(0);};

	for (size_t i = 2; i < 5; ++i) {
		sim[2 * i]		<< [&]{clock.write(1);};
		sim[2 * i + 1]	<< [&]{clock.write(0);};
	}

	sim.run(0, 12, 1);

	logger.create_log();
}

int main() {
	std::vector<std::function<void()>> tests = {test1};
	for (auto t: tests) t();
	return 0;
};
