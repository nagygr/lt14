/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_THERMAL_EQUIVALENTCIRCUITTESTER
#define ENGINE_THERMAL_EQUIVALENTCIRCUITTESTER

#include <util/tester.h>

namespace test {
	class equivalent_circuit_tester : public util::tester {
		public:
			equivalent_circuit_tester() {
				add_test(__FILE__, &equivalent_circuit_tester::test_linkage);
				add_test(__FILE__, &equivalent_circuit_tester::test_logithermal_simulation);
			}

			static util::tester::test_result test_linkage();
			static util::tester::test_result test_logithermal_simulation();
	};
}

#endif // ENGINE_THERMAL_EQUIVALENTCIRCUITTESTER
