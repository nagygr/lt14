/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <tuple>
#include <vector>
#include <memory>
#include <string>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <functional>

#include <test/engine/thermal/equivalent_circuit_tester.h>

#include <engine/logic/bit.h>
#include <quantity/time_type.h>
#include <engine/logic/signal.h>
#include <engine/logic/component.h>
#include <engine/logic/bit_array.h>
#include <engine/logic/simulation.h>
#include <engine/logic/output_port.h>
#include <engine/logic/delta_input_port.h>
#include <engine/logic/vcd_event_logger.h>
#include <engine/logic/ostream_event_logger.h>
#include <engine/logic/sensitive_input_port.h>
#include <engine/thermal/layout_information.h>
#include <engine/thermal/equivalent_circuit/thermal_simulator.h>
#include <engine/thermal/equivalent_circuit/layout_information.h>

namespace test {
	using namespace engine::thermal;
	using namespace engine::logic;

	template <size_t data_size>
	class counter : public component {
		public:
			sensitive_input_port<bit> clock;
			delta_input_port<bit> reset;
			output_port<bit_array<data_size>> output;

		public:
			counter(std::string const &name) : component(name) {
				clock << std::bind(&counter::operation, this);
				ports() << clock << reset << output;
			}

			void operation() {
				if (clock.read() == '1') {
					if (reset.read() == '1') {
						output.write(0);
					}
					else {
						try {
							output.write(output.read().to_unsigned() + 1);
						}
						catch (std::domain_error const &) {
							output.write(bit_array<data_size>());
						}
					}
				}
			}
	};

	template <size_t data_size>
	class comparator : public component {
		public:
			sensitive_input_port<bit_array<data_size>> data_a;
			sensitive_input_port<bit_array<data_size>> data_b;
			output_port<bit> output;

		public:
			comparator(std::string const &name) : component(name) {
				data_a << std::bind(&comparator::operation, this);
				data_b << std::bind(&comparator::operation, this);
				ports() << data_a << data_b << output;
			}

			void operation() {
				bool result = data_a.read() == data_b.read();
				output.write(result ? '1' : '0');
			}
	};

	class or_gate : public component {
		public:
			sensitive_input_port<bit> input_a;
			sensitive_input_port<bit> input_b;
			output_port<bit> output;

		public:
			or_gate(std::string const &name) : component(name) {
				input_a << std::bind(&or_gate::operation, this);
				input_b << std::bind(&or_gate::operation, this);
				ports() << input_a << input_b << output;
			}

			void operation() {
				output.write(input_a.read() | input_b.read()); 
			}
	};


	util::tester::test_result equivalent_circuit_tester::test_linkage() {
		equivalent_circuit::layout_information layout_information(std::make_tuple(3, 4), std::make_tuple(2, 1));	

		return true;
	}

	util::tester::test_result equivalent_circuit_tester::test_logithermal_simulation() {
		//Definition of the circuit
		signal<bit> clock;
		signal<bit> reset;
		signal<bit_array<8>> data;

		counter<8> cnt("Counter");
		comparator<8> cmp("Comparator");
		or_gate org("Or gate");

		vcd_event_logger logger("test_data/test_logithermal_simulation.vcd");

		clock.connect(cnt.clock);
		reset.connect(org.input_a);
		data.connect(cmp.data_b);
		cnt.output.connect(cmp.data_a);
		cmp.output.connect(org.input_b);
		org.output.connect(cnt.reset);

		clock.connect(logger.log_as<bit>("clock"));
		reset.connect(logger.log_as<bit>("reset"));
		data.connect(logger.log_as<bit_array<8>>("data"));
		cnt.output.connect(logger.log_as<bit_array<8>>("counter"));
		cmp.output.connect(logger.log_as<bit>("comparator"));
		org.output.connect(logger.log_as<bit>("orgate"));

		//Definition of the thermal environment
		quantity::time_type time_step = 1;
		layout_information::coordinates layout_size(50, 50);

		std::vector<std::tuple<std::string, std::shared_ptr<layout_information>>> layout;

		layout.push_back(std::tuple<std::string, std::shared_ptr<layout_information>>("Counter", std::make_shared<equivalent_circuit::layout_information>(layout_size, layout_information::coordinates(4, 10))));
		layout.push_back(std::tuple<std::string, std::shared_ptr<layout_information>>("Comparator", std::make_shared<equivalent_circuit::layout_information>(layout_size, layout_information::coordinates(28, 26))));
		layout.push_back(std::tuple<std::string, std::shared_ptr<layout_information>>("Or gate", std::make_shared<equivalent_circuit::layout_information>(layout_size, layout_information::coordinates(48, 20))));

		equivalent_circuit::thermal_simulator thermal_simulator(
			component::get_components(),				//std::vector<engine::logic::component *> &components,
			layout,                                     //std::vector<std::tuple<std::string,std::shared_ptr<layout_information>>> &layout,
			layout_size,								//layout_information::coordinates const &layout_size,
			1e-6,                                       //double grid_size_x,
			1e-6,                                       //double grid_size_y,
			3e-4,                                       //double wafer_thickness,
			time_step                                   //quantity::time_type time_step
		);

		cnt.set_constant_dissipation(3e-6);
		cmp.set_constant_dissipation(6e-6);
		org.set_constant_dissipation(1e-7);

		std::cerr << "Before the simulation: " << std::endl;
		std::cerr << "Counter: " << cnt.get_temperature() << " °C" << std::endl;
		std::cerr << "Comparator: " << cmp.get_temperature() << " °C" << std::endl;
		std::cerr << "Or gate: " << org.get_temperature() << " °C" << std::endl;

		//Setting up the simulation
		simulation sim;

		sim.set_thermal_simulator(thermal_simulator);

		sim[0]	<< [&]{reset.write('0');}
				<< [&]{data.write(25);};

		sim[10]	<< [&]{reset.write('1');};

		sim[25]	<< [&]{reset.write('0');};

		size_t length = 800;
		size_t clock_period = 10;

		for (size_t i = 0; i < length / clock_period; ++i) {
			sim[clock_period * i]						<< [&]{clock.write('1');};
			sim[clock_period * i + clock_period / 2]	<< [&]{clock.write('0');};
		}

		sim.run(0, length, time_step, 25.0);

		logger.create_log();

		std::cerr << std::endl << "After the simulation: " << std::endl;
		std::cerr << "Counter: " << cnt.get_temperature() << " °C" << std::endl;
		std::cerr << "Comparator: " << cmp.get_temperature() << " °C" << std::endl;
		std::cerr << "Or gate: " << org.get_temperature() << " °C" << std::endl;

		std::fstream image("test_data/test_logithermal_simulation.svg", std::fstream::out);
		if (image) thermal_simulator.write_on_stream(image);

		return true;
	}

}
