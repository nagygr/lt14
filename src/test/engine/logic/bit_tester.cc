/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <iostream>
#include <array>

#include <engine/logic/bit.h>
#include <engine/logic/bit_array.h>
#include <test/engine/logic/bit_tester.h>

namespace test {
	util::tester::test_result bit_tester::test_constructors() {
		using engine::logic::bit;

		bit b1, b2(bit::value::high), b3('1');

		if (b1.get_value() != bit::value::undefined) return util::tester::test_result(false, "Bit expected to be undefined");
		if (b2.get_value() != bit::value::high) return util::tester::test_result(false, "Bit expected to be high");
		if (b3.get_value() != bit::value::high) return util::tester::test_result(false, "Bit expected to be high");

		std::cout << "B1: " << b1 << std::endl;
		std::cout << "B2: " << b2 << std::endl;
		std::cout << "B3: " << b3 << std::endl;

		return true;
	}

	util::tester::test_result bit_tester::test_operators() {
		using engine::logic::bit;

		bit b1, b2, b3;

		b3 = b1 & b2;

		std::cout << b1 << " " << b2 << " " << b3 << std::endl;
		if (b3.get_value() != bit::value::undefined) return util::tester::test_result(false, "Bit expected to be undefined");

		b1 = bit::value::high;
		b2 = bit::value::low;

		if ((b1 & b2).get_value() != bit::value::low) return util::tester::test_result(false, "Bit expected to be low");
		if ((b1 | b2).get_value() != bit::value::high) return util::tester::test_result(false, "Bit expected to be high");
		if ((b1 ^ b2).get_value() != bit::value::high) return util::tester::test_result(false, "Bit expected to be high");

		if ((b1 ^ b2) != bit::value::high) return util::tester::test_result(false, "Bit expected to be high");

		/*
		//TRUTH TABLES:
		for (auto v1 : {bit::value::undefined, bit::value::low, bit::value::high, bit::value::high_impedance}) {
			for (auto v2 : {bit::value::undefined, bit::value::low, bit::value::high, bit::value::high_impedance}) {
				b1 = v1; b2 = v2;
				std::cout << b1 << " & " << b2 << ": " << (b1 & b2) << std::endl;
			}
		}

		std::cout << std::endl;

		for (auto v1 : {bit::value::undefined, bit::value::low, bit::value::high, bit::value::high_impedance}) {
			for (auto v2 : {bit::value::undefined, bit::value::low, bit::value::high, bit::value::high_impedance}) {
				b1 = v1; b2 = v2;
				std::cout << b1 << " | " << b2 << ": " << (b1 | b2) << std::endl;
			}
		}

		std::cout << std::endl;
		
		for (auto v1 : {bit::value::undefined, bit::value::low, bit::value::high, bit::value::high_impedance}) {
			for (auto v2 : {bit::value::undefined, bit::value::low, bit::value::high, bit::value::high_impedance}) {
				b1 = v1; b2 = v2;
				std::cout << b1 << " ^ " << b2 << ": " << (b1 ^ b2) << std::endl;
			}
		}
		*/

		return true;
	}
	
	util::tester::test_result bit_tester::test_user_literal() {
		using engine::logic::bit;

		bit b1 = "0"_b, b2 = "1"_b, b3 = "x"_b, b4 = "z"_b;

		if (b1 != bit::value::low) return util::tester::test_result(false, "Bit expected to be bit::value::low");
		if (b2 != bit::value::high) return util::tester::test_result(false, "Bit expected to be bit::value::high");
		if (b3.get_value() != bit::value::undefined) return util::tester::test_result(false, "Bit expected to be bit::value::undefined"); //get_value is need because two undefined bits are not equal
		if (b4.get_value() != bit::value::high_impedance) return util::tester::test_result(false, "Bit expected to be bit::value::high_impedance"); //get_value is need because two high_impedance bits are not equal

		return true;
	}

	util::tester::test_result bit_tester::test_bit_array() {
		using engine::logic::bit;
		using engine::logic::bit_array;

		bit_array<8> b1("0110xzxz");
		bit_array<8> b2("0011xz10");
		bit_array<8> b3;

		std::cout << b1 << " " << b2 << " " << b3 << std::endl;
		
		std::cout << (b3 = b1 & b2) << std::endl;
		if (b3[5] != bit::value::high) return util::tester::test_result(false, "Bit expected to be high");

		std::cout << (b3 = b1 | b2) << std::endl;
		if (b3[4] != bit::value::high) return util::tester::test_result(false, "Bit expected to be high");
		
		std::cout << (b3 = b1 ^ b2) << std::endl;
		if (b3[6] != bit::value::high) return util::tester::test_result(false, "Bit expected to be high");

		bit_array<8> b4(254);
		std::cout << b4 << std::endl;
		if (b4[1] != bit::value::high) return util::tester::test_result(false, "Bit expected to be high");

		bit_array<8> b5(12);
		unsigned long long value = b5.to_unsigned();
		std::cout << value << std::endl;
		if (value != 12) return util::tester::test_result(false, "Value of bit_array expected to be 12");

		b3 = b4 / b5;
		std::cout << b3 << " " << b3.to_unsigned() << std::endl;
		if (b3.to_unsigned()!= 21) return util::tester::test_result(false, "Value of bit_array expected to be 21");

		bit_array<8> b6 = "1xx00110";
		std::cout << "b6: " << b6 << " " << b6[6] << std::endl;
		if (b6[6].get_value() != bit::value::undefined) return util::tester::test_result(false, "Bit expected to be undefined");

		bit_array<8> b7 = bit_array<8>(bit('0'));
		std::cout << "b7: " << b7 << " " << b7[5] << std::endl;
		if (b7[5].get_value() != bit::value::low) return util::tester::test_result(false, "Bit expected to be low");

		bit_array<8> b8 = bit_array<8>(1);
		std::cout << "b8: " << b8 << " " << b8[5] << std::endl;
		if (b8[5].get_value() != bit::value::low) return util::tester::test_result(false, "Bit expected to be low");

		bit_array<8> b9(1), b10;
		std::cout << "b9: " << b9 << " b10: " << b10 << std::endl;
		bit_array<8> b11 = b9 + b10;
		std::cout << "b9 + b10 = " << b11 << std::endl;
		if (b11[3].get_value() != bit::value::undefined) return util::tester::test_result(false, "Bit expected to be undefined");

		return true;
	}
}
