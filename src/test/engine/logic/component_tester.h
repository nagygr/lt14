/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef TEST_ENGINE_LOGIC_COMPONENTTESTER
#define TEST_ENGINE_LOGIC_COMPONENTTESTER

#include <util/tester.h>

namespace test {
	class component_tester : public util::tester {
		public:
			component_tester() {
				add_test(__FILE__, &component_tester::test_counter);
				add_test(__FILE__, &component_tester::test_combinational);
				add_test(__FILE__, &component_tester::test_dff);
				add_test(__FILE__, &component_tester::test_delta);
				add_test(__FILE__, &component_tester::test_dff_bit);
				add_test(__FILE__, &component_tester::test_counter_bit_array);
			}

			static util::tester::test_result test_counter();
			static util::tester::test_result test_combinational();
			static util::tester::test_result test_dff();
			static util::tester::test_result test_delta();
			static util::tester::test_result test_dff_bit();
			static util::tester::test_result test_counter_bit_array();
	};
}

#endif //TEST_ENGINE_LOGIC_COMPONENTTESTER
