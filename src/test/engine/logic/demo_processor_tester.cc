/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <vector>
#include <memory>
#include <algorithm>
#include <stdexcept>
#include <functional>

#include <engine/logic/bit.h>
#include <quantity/time_type.h>
#include <engine/logic/signal.h>
#include <engine/logic/component.h>
#include <engine/logic/bit_array.h>
#include <engine/logic/simulation.h>
#include <engine/logic/output_port.h>
#include <engine/logic/delta_input_port.h>
#include <engine/logic/vcd_event_logger.h>
#include <engine/logic/ostream_event_logger.h>
#include <engine/logic/sensitive_input_port.h>

#include <test/engine/logic/demo_processor_tester.h>

namespace test {
	using namespace engine::logic;
	using quantity::time_type;

	template <size_t size>
	class standard_register : public component {
		public:
			delta_input_port<bit_array<size>> din;
			sensitive_input_port<bit> clk;
			delta_input_port<bit> reset;
			delta_input_port<bit> ce;

			output_port<bit_array<size>> dout;
			
		public:
			standard_register(std::string const &name) : component(name), clk(std::bind(&standard_register::operation, this)) {
				ports() << din << clk << reset << ce << dout;
			}

			void operation() {
				if (clk.read() == '1') {
					if (reset.read() == '1') {
						dout.write(bit_array<size>(bit('0')));
					}
					else if (ce.read() == '1') {
						dout.write(din.read());
					}
				}
			}
	};

	template <size_t size>
	class adder : public component {
		public:
			sensitive_input_port<bit_array<size>> in1;
			sensitive_input_port<bit_array<size>> in2;
			output_port<bit_array<size>> result;

		public:
			adder(std::string const &name) : component(name), in1(std::bind(&adder::operation, this)), in2(std::bind(&adder::operation, this)) {
				ports() << in1 << in2 << result;
			}

			void operation() {
				result.write(in1.read() + in2.read());
			}
	};

	template <size_t size, size_t msb, size_t lsb>
	class range_selector : public component {
		public:
			static constexpr size_t output_size = msb - lsb + 1;

			sensitive_input_port<bit_array<size>> in;
			output_port<bit_array<output_size>> out;

		public:
			range_selector(std::string const &name) : component(name), in(std::bind(&range_selector::operation, this)) {
				static_assert(lsb <= msb, "The value of lsb has to be smaller than or equal to that of msb in range_selector.");
				ports() << in << out;
			}

			void operation() {
				bit_array<output_size> result;

				for (size_t i = 0; i < output_size; ++i) {
					result[i] = in.read()[i + lsb];
				}

				out.write(result);
			}
	};

	template <size_t data_size, size_t address_width>
	class multiplexer : public component {
		public:
			std::vector<std::shared_ptr<sensitive_input_port<bit_array<data_size>>>> inputs;
			sensitive_input_port<bit_array<address_width>> selector;
			output_port<bit_array<data_size>> out;

		public:
			multiplexer(std::string const &name) : component(name), selector(std::bind(&multiplexer::operation, this)) {
				for (size_t i = 0; i < (1 << address_width); ++i) {
					inputs.push_back(std::make_shared<sensitive_input_port<bit_array<data_size>>>(std::bind(&multiplexer::operation, this)));
					ports() << *inputs[inputs.size() - 1];
				}

				ports() << out;
			}

			void operation() {
				try {
					out.write(inputs[selector.read().to_unsigned()]->read());
				}
				catch (std::domain_error const &) {
					out.write(bit_array<data_size>());
				}
			}
	};

	template <size_t data_size, size_t address_width>
	class regfile : public component {
		public:
			static constexpr size_t memory_size = 1 << address_width;

		private:
			std::array<bit_array<data_size>, memory_size> data_;

		public:
			sensitive_input_port<bit> clk;
			delta_input_port<bit> write_enable;
			delta_input_port<bit_array<address_width>> write_address;
			delta_input_port<bit_array<data_size>> data_in;
			delta_input_port<bit_array<address_width>> address_a;
			delta_input_port<bit_array<address_width>> address_b;
			delta_input_port<bit_array<address_width>> address_c;
			output_port<bit_array<data_size>> data_out_a;
			output_port<bit_array<data_size>> data_out_b;
			output_port<bit_array<data_size>> data_out_c;

			regfile(std::string const &name) : component(name) {
				clk << std::bind(&regfile::write, this) << std::bind(&regfile::read_a, this) << std::bind(&regfile::read_b, this) << std::bind(&regfile::read_c, this);
				ports() << clk << write_enable << write_address << data_in << address_a << address_b << address_c << data_out_a << data_out_b << data_out_c;
			}

			regfile(std::string const &name, std::array<bit_array<data_size>, memory_size> const &initializer) : regfile(name) {
				std::copy(initializer.begin(), initializer.end(), data_.begin());
			}

			void write() {
				if (clk.read() == '1' && write_enable.read() == '1') { 
					data_[write_address.read().to_unsigned()] = data_in.read(); //This operation will throw std::domain_error if write_address is undefined
				}
			}

			void read_a() {
				try {
					if (clk.read() == '1') data_out_a.write(data_[address_a.read().to_unsigned()]);
				} catch (std::domain_error const &) {
					data_out_a.write(bit_array<data_size>());
				}
			}

			void read_b() {
				try {
					if (clk.read() == '1') data_out_b.write(data_[address_b.read().to_unsigned()]);
				} catch (std::domain_error const &) {
					data_out_b.write(bit_array<data_size>());
				}
			}

			void read_c() {
				try {
					if (clk.read() == '1') data_out_c.write(data_[address_c.read().to_unsigned()]);
				} catch (std::domain_error const &) {
					data_out_c.write(bit_array<data_size>());
				}
			}
	};

	template <size_t data_size>
	class alu : component {
		public:
			enum class function {alu_add, alu_sub, alu_and, alu_or, alu_xor, alu_xxx = 7};

			sensitive_input_port<bit_array<3>> func;
			sensitive_input_port<bit_array<data_size>> op_a;
			sensitive_input_port<bit_array<data_size>> op_b;
			output_port<bit_array<data_size>> result;
			output_port<bit_array<1>> z_out;
			output_port<bit_array<1>> n_out;

			alu(std::string const &name) : component(name), func(std::bind(&alu::operation, this)), op_a(std::bind(&alu::operation, this)), op_b(std::bind(&alu::operation, this)) {
				ports() << func << op_a << op_b << result << z_out << n_out;
			}
			
			void operation() {
				function current_function;
				try {
					current_function = function(func.read().to_unsigned());
				} catch (std::domain_error const &de) {
					current_function = function::alu_xxx;
				}

				unsigned long long value;
				bit_array<data_size> bit_array_value;

				//std::cerr << "ALU function: " << static_cast<unsigned>(current_function) << std::endl;

				switch (current_function) {
					case function::alu_add:
						try {
							value = op_a.read().to_unsigned() + op_b.read().to_unsigned();
							bit_array<data_size> the_result(value & ((1 << data_size) - 1));
							result.write(the_result);
							z_out.write(the_result == bit_array<data_size>('0'));
							//n_out.write(((value & (1 << data_size)) != 0) ? "1" : "0");
							n_out.write(the_result[(data_size - 1)] == bit('1') ? "1": "0");
						} catch (std::domain_error const &de) {
							result.write(bit_array<data_size>('x'));
							z_out.write("x");
							n_out.write("x");
						}
						break;

					case function::alu_sub:
						try {
							value = op_a.read().to_unsigned() - op_b.read().to_unsigned();
							bit_array<data_size> the_result(value & ((1 << data_size) - 1));
							result.write(the_result);
							z_out.write(the_result == bit_array<data_size>('0'));
							n_out.write(the_result[(data_size - 1)] == bit('1') ? "1": "0");
							/*
							result.write(bit_array<data_size>(value & ((1 << data_size) - 1)));
							z_out.write((value == 0) ? "1" : "0");
							n_out.write(((value & (1 << data_size)) != 0) ? "1" : "0");
							*/
						} catch (std::domain_error const &de) {
							result.write(bit_array<data_size>('x'));
							z_out.write("x");
							n_out.write("x");
						}
						break;

					case function::alu_and:
						result.write(bit_array_value = op_a.read() & op_b.read());
						z_out.write(bit_array_value == bit_array<data_size>(0) ? "1" : "0");
						n_out.write(bit_array_value[(data_size - 1)] == bit('1') ? "1": "0");
						break;

					case function::alu_or:
						result.write(bit_array_value = op_a.read() | op_b.read());
						z_out.write(bit_array_value  == bit_array<data_size>(0) ? "1" : "0");
						n_out.write(bit_array_value[(data_size - 1)] == bit('1') ? "1": "0");
						break;

					case function::alu_xor:
						result.write(bit_array_value = op_a.read() ^ op_b.read());
						z_out.write(bit_array_value  == bit_array<data_size>(0) ? "1" : "0");
						n_out.write(bit_array_value[(data_size - 1)] == bit('1') ? "1": "0");
						break;

					default:
						result.write(bit_array<data_size>(op_b.read()));
						z_out.write(op_b.read() == bit_array<data_size>(0) ? "1" : "0");
						n_out.write(op_b.read()[(data_size - 1)] == bit('1') ? "1": "0");

				}
			}
	};

	class instruction_decoder : public component {
		public:
			enum class instruction {i_i2rf, i_m2rf, i_rf2m, i_add, i_sub, i_and, i_or, i_xor, i_jmp, i_bz, i_bn, i_eoj};

			sensitive_input_port<bit_array<4>> in;
			output_port<bit_array<3>> alu_func;

		public:
			instruction_decoder(std::string const &name) : component(name) {
				in << std::bind(&instruction_decoder::operation, this);
				ports() << in << alu_func;
			}

			void operation() {
				try {
					switch(instruction(in.read().to_unsigned())) {
						case instruction::i_add:
							alu_func.write(bit_array<3>(static_cast<int>(alu<8>::function::alu_add)));
							break;

						case instruction::i_sub:
							alu_func.write(bit_array<3>(static_cast<int>(alu<8>::function::alu_sub)));
							break;

						case instruction::i_and:
							alu_func.write(bit_array<3>(static_cast<int>(alu<8>::function::alu_and)));
							break;

						case instruction::i_or:
							alu_func.write(bit_array<3>(static_cast<int>(alu<8>::function::alu_or)));
							break;

						case instruction::i_xor:
							alu_func.write(bit_array<3>(static_cast<int>(alu<8>::function::alu_xor)));
							break;

						default:
							alu_func.write(bit_array<3>(static_cast<int>(alu<8>::function::alu_xxx)));
					}
				}
				catch (std::domain_error const &) {
					alu_func.write(bit_array<3>());
				}
			}
	};

	class controller : public component {
		public:
			enum class state {wait_for_run, fetch_1, fetch_2, decode_1, decode_2, execute_1, execute_2, data_cache_read_access, inc_IP, deassert_all};

			sensitive_input_port<bit> clk;
			delta_input_port<bit> reset;
			delta_input_port<bit> run;
			delta_input_port<bit_array<1>> ss_Z;
			delta_input_port<bit_array<1>> ss_N;
			delta_input_port<bit_array<4>> ss_instruction_code;
			output_port<bit> end_of_job;
			output_port<bit> write_data_cache;
			output_port<bit> ce_IR;
			output_port<bit> ce_IP;
			output_port<bit> ce_instruction_code;
			output_port<bit> ce_destination;
			output_port<bit> ce_OP_A;
			output_port<bit> ce_OP_B;
			output_port<bit> ce_IMM;
			output_port<bit> ce_MAR;
			output_port<bit> ce_MDR;
			output_port<bit> ce_Z;
			output_port<bit> ce_N;
			output_port<bit> we_RF;
			output_port<bit_array<1>> sel_IP;
			output_port<bit_array<1>> sel_IP_add;
			output_port<bit_array<2>> sel_ALU;

		private:
			state state_;

		public:
			controller(std::string const &name) : component(name) {
				clk << std::bind(&controller::operation, this);
				ports() << clk << reset << run << ss_Z << ss_N << ss_instruction_code << end_of_job << write_data_cache << ce_IR << ce_IP << ce_instruction_code << ce_destination << ce_OP_A
					<< ce_OP_B << ce_IMM << ce_MAR << ce_MDR << ce_Z << ce_N << we_RF << sel_IP << sel_IP_add << sel_ALU;

				state_ = state::wait_for_run;
			}

			void operation() {
				if (clk.read() == '1') {
					if (reset.read() == '1') {
						state_ = state::wait_for_run;
						end_of_job.write('0');
						write_data_cache.write('0');
						ce_IR.write('0');
						ce_IP.write('0');
						ce_instruction_code.write('0');
						ce_destination.write('0');
						ce_OP_A.write('0');
						ce_OP_B.write('0');
						ce_IMM.write('0');
						ce_MAR.write('0');
						ce_MDR.write('0');
						ce_Z.write('0');
						ce_N.write('0');
						we_RF.write('0');
						sel_IP.write('0');
						sel_IP_add.write('0');
						sel_ALU.write("00");
					}
					else {
						switch(state_) {
							case state::wait_for_run:
								if (run.read() == '1') {
									end_of_job.write('0');
									sel_IP.write("0");
									ce_IP.write('1');
									state_ = state::fetch_1;
								}
								else state_ = state::wait_for_run;
								break;

							case state::fetch_1:
								ce_IP.write('0');
								state_ = state::fetch_2;
								break;

							case state::fetch_2:
								ce_IR.write('1');
								state_ = state::decode_1;
								break;

							case state::decode_1:
								ce_IR.write('0');
								state_ = state::decode_2;
								break;

							case state::decode_2:
								ce_instruction_code.write('1');
								ce_destination.write('1');
								ce_OP_A.write('1');
								ce_OP_B.write('1');
								ce_IMM.write('1');
								ce_MAR.write('1');
								ce_MDR.write('1');

								state_ = state::execute_1;
								break;

							case state::execute_1:
								ce_instruction_code.write('0');
								ce_destination.write('0');
								ce_OP_A.write('0');
								ce_OP_B.write('0');
								ce_IMM.write('0');
								ce_MAR.write('0');
								ce_MDR.write('0');

								state_ = state::execute_2;
								break;

							case state::execute_2:
								state_ = state::inc_IP;

								if (ss_instruction_code.read() == bit_array<4>(static_cast<int>(instruction_decoder::instruction::i_i2rf))) {
									sel_ALU.write("00");
									we_RF.write('1');
									ce_Z.write('1');
									ce_N.write('1');
								} else if (ss_instruction_code.read() == bit_array<4>(static_cast<int>(instruction_decoder::instruction::i_m2rf))) {
									state_ = state::data_cache_read_access;
								} else if (ss_instruction_code.read() == bit_array<4>(static_cast<int>(instruction_decoder::instruction::i_rf2m))) {
									write_data_cache.write('1');
								} else if (ss_instruction_code.read() == bit_array<4>(static_cast<int>(instruction_decoder::instruction::i_add)) ||
										ss_instruction_code.read() == bit_array<4>(static_cast<int>(instruction_decoder::instruction::i_sub)) ||
										ss_instruction_code.read() == bit_array<4>(static_cast<int>(instruction_decoder::instruction::i_and)) ||
										ss_instruction_code.read() == bit_array<4>(static_cast<int>(instruction_decoder::instruction::i_or)) ||
										ss_instruction_code.read() == bit_array<4>(static_cast<int>(instruction_decoder::instruction::i_xor))
										) {
									sel_ALU.write("10");
									we_RF.write('1');
									ce_Z.write('1');
									ce_N.write('1');
								} else if (ss_instruction_code.read() == bit_array<4>(static_cast<int>(instruction_decoder::instruction::i_jmp))) {
									sel_IP.write("1");
									sel_IP_add.write("1");
									ce_IP.write('1');
									state_ = state::deassert_all;
								} else if (ss_instruction_code.read() == bit_array<4>(static_cast<int>(instruction_decoder::instruction::i_bz))) {
									if (ss_Z.read() == "1") {
										sel_IP_add.write("1");
									} else {
										sel_IP_add.write("0");
									}
									sel_IP.write("1");
									ce_IP.write('1');
									state_ = state::deassert_all;
								} else if (ss_instruction_code.read() == bit_array<4>(static_cast<int>(instruction_decoder::instruction::i_bn))) {
									if (ss_N.read() == "1") {
										sel_IP_add.write("1");
									} else {
										sel_IP_add.write("0");
									}
									sel_IP.write("1");
									ce_IP.write('1');
									state_ = state::deassert_all;
								} else if (ss_instruction_code.read() == bit_array<4>(static_cast<int>(instruction_decoder::instruction::i_eoj))) {
									end_of_job.write('1');
									state_ = state::wait_for_run;
								}
								break;

							case state::data_cache_read_access:
								sel_ALU.write("01");
								we_RF.write('1');
								ce_Z.write('1');
								ce_N.write('1');
								state_ = state::inc_IP;
								break;

							case state::inc_IP:
								sel_IP.write("1");
								sel_IP_add.write("0");
								ce_IP.write('1');
								state_ = state::deassert_all;
								break;

							case state::deassert_all:
								ce_IR.write('0');
								ce_IP.write('0');
								ce_instruction_code.write('0');
								ce_destination.write('0');
								ce_OP_A.write('0');
								ce_OP_B.write('0');
								ce_IMM.write('0');
								ce_MAR.write('0');
								ce_MDR.write('0');
								ce_Z.write('0');
								ce_N.write('0');
								we_RF.write('0');
								write_data_cache.write('0');
								state_ = state::fetch_2;
							default:
								break;
						}
					}
				}
			}
	};

	util::tester::test_result demo_processor_tester::test_flipflop() {
		signal<bit_array<8>> din;
		signal<bit> clk;
		signal<bit> reset;
		signal<bit> ce;

		standard_register<8> reg1("reg1"), reg2("reg2");

		vcd_event_logger logger("test_data/test_standard_register.vcd");

		clk.connect(reg1.clk);
		reset.connect(reg1.reset);
		ce.connect(reg1.ce);
		din.connect(reg1.din);

		clk.connect(reg2.clk);
		reset.connect(reg2.reset);
		ce.connect(reg2.ce);
		reg1.dout.connect(reg2.din);

		clk.connect(logger.log_as<bit>("clock"));
		reset.connect(logger.log_as<bit>("reset"));
		ce.connect(logger.log_as<bit>("ce"));
		din.connect(logger.log_as<bit_array<8>>("din"));
		reg1.dout.connect(logger.log_as<bit_array<8>>("reg1_dout"));
		reg2.dout.connect(logger.log_as<bit_array<8>>("reg2_dout"));

		simulation sim;

		sim[0]	<< [&]{reset.write('0');} << [&]{ce.write('0');} << [&]{din.write(bit_array<8>(bit('0')));}; 

		sim[32] << [&]{reset.write('1');}; 

		sim[43] << [&]{reset.write('0');}; 

		sim[62] << [&]{ce.write('1');};

		sim[82] << [&]{din.write("01101100");};

		sim[130] << [&]{reset.write('1');}; 

		sim[140] << [&]{reset.write('0');}; 

		for (time_type t = 0; t < 200; t += 10) {
			if (t % 20 == 0) sim[t] << [&]{clk.write('1');};
			else sim[t] << [&]{clk.write('0');};
		}

		sim.run(0, 210, 1);

		logger.create_log();
		return true;
	}

	util::tester::test_result demo_processor_tester::test_adder() {
		signal<bit_array<8>> in1;
		signal<bit_array<8>> in2;

		adder<8> add1("adder");

		vcd_event_logger logger("test_data/test_adder.vcd");

		in1.connect(add1.in1);
		in2.connect(add1.in2);

		in1.connect(logger.log_as<bit_array<8>>("in1"));
		in2.connect(logger.log_as<bit_array<8>>("in2"));
		add1.result.connect(logger.log_as<bit_array<8>>("adder"));

		simulation sim;

		sim[0] << [&]{in1.write(bit_array<8>(bit('0')));} << [&]{in2.write(bit_array<8>(bit('0')));};

		sim[5] << [&]{in1.write(bit_array<8>("00101100"));} << [&]{in2.write(bit_array<8>("01011011"));};

		sim[10] << [&]{in1.write(bit_array<8>("11111110"));} << [&]{in2.write(bit_array<8>("00000001"));};

		sim[15] << [&]{in1.write(bit_array<8>("11111111"));} << [&]{in2.write(bit_array<8>("00000001"));};

		sim[20] << [&]{in1.write(bit_array<8>("00000000"));} << [&]{in2.write(bit_array<8>("00000000"));};

		sim.run(0, 25, 1);

		logger.create_log();

		return true;
	}

	util::tester::test_result demo_processor_tester::test_range_selector() {
		signal<bit_array<8>> in;

		range_selector<8, 5, 2> rs1("range_selector");

		vcd_event_logger logger("test_data/test_range_selector.vcd");

		in.connect(rs1.in);

		in.connect(logger.log_as<bit_array<8>>("in"));
		rs1.out.connect(logger.log_as<bit_array<4>>("range_selector_5_2"));

		simulation sim;

		sim[0] << [&]{in.write(bit_array<8>(bit('0')));};

		sim[4] << [&]{in.write(bit_array<8>("01101001"));};

		sim[8] << [&]{in.write(bit_array<8>("00001111"));};

		sim[12] << [&]{in.write(bit_array<8>("01010101"));};

		sim[16] << [&]{in.write(bit_array<8>("00110011"));};

		sim[24] << [&]{in.write(bit_array<8>("00000000"));};

		sim.run(0, 25, 1);

		logger.create_log();

		return true;
	}

	util::tester::test_result demo_processor_tester::test_multiplexer() {
		signal<bit_array<8>> in1;
		signal<bit_array<8>> in2;
		signal<bit_array<8>> in3;
		signal<bit_array<8>> in4;

		signal<bit_array<2>> selector;

		multiplexer<8, 2> mux("multiplexer");

		vcd_event_logger logger("test_data/test_multiplexer.vcd");

		in1.connect(*mux.inputs[0]);
		in2.connect(*mux.inputs[1]);
		in3.connect(*mux.inputs[2]);
		in4.connect(*mux.inputs[3]);
		selector.connect(mux.selector);

		in1.connect(logger.log_as<bit_array<8>>("in1"));
		in2.connect(logger.log_as<bit_array<8>>("in2"));
		in3.connect(logger.log_as<bit_array<8>>("in3"));
		in4.connect(logger.log_as<bit_array<8>>("in4"));
		selector.connect(logger.log_as<bit_array<2>>("selector"));
		mux.out.connect(logger.log_as<bit_array<8>>("multiplexer_8_2"));

		simulation sim;

		sim[0] <<	[&]{selector.write(bit_array<2>(bit('0')));} <<
			[&]{in1.write(bit_array<8>(1));} <<
			[&]{in2.write(bit_array<8>(2));} <<
			[&]{in3.write(bit_array<8>(3));} <<
			[&]{in4.write(bit_array<8>(4));};

		sim[4] << [&]{selector.write(bit_array<2>(1));};

		sim[8] << [&]{selector.write(bit_array<2>(2));};

		sim[12] << [&]{selector.write(bit_array<2>(3));};

		sim[16] << [&]{selector.write(bit_array<2>(0));};

		sim.run(0, 25, 1);

		logger.create_log();

		return true;
	}

	util::tester::test_result demo_processor_tester::test_regfile() {
		signal<bit> clk;
		signal<bit> write_enable;
		signal<bit_array<8>> write_address;
		signal<bit_array<8>> data_in;
		signal<bit_array<8>> address_a;
		signal<bit_array<8>> address_b;
		signal<bit_array<8>> address_c;

		regfile<8, 8> rf("rf");

		vcd_event_logger logger("test_data/test_regfile.vcd");

		clk.connect(rf.clk);
		write_enable.connect(rf.write_enable);
		write_address.connect(rf.write_address);
		data_in.connect(rf.data_in);
		address_a.connect(rf.address_a);
		address_b.connect(rf.address_b);
		address_c.connect(rf.address_c);

		clk.connect(logger.log_as<bit>("clk"));
		write_enable.connect(logger.log_as<bit>("write_enable"));
		write_address.connect(logger.log_as<bit_array<8>>("write_address"));
		data_in.connect(logger.log_as<bit_array<8>>("data_in"));
		address_a.connect(logger.log_as<bit_array<8>>("address_a"));
		address_b.connect(logger.log_as<bit_array<8>>("address_b"));
		address_c.connect(logger.log_as<bit_array<8>>("address_c"));
		rf.data_out_a.connect(logger.log_as<bit_array<8>>("data_out_a"));
		rf.data_out_b.connect(logger.log_as<bit_array<8>>("data_out_b"));
		rf.data_out_c.connect(logger.log_as<bit_array<8>>("data_out_c"));

		simulation sim;

		sim[0] <<	[&]{write_enable.write('0');} <<
			[&]{write_address.write(bit_array<8>(0));} <<
			[&]{data_in.write(bit_array<8>(0));} <<
			[&]{address_a.write(bit_array<8>(0));} <<
			[&]{address_b.write(bit_array<8>(0));} <<
			[&]{address_c.write(bit_array<8>(0));};

		sim[13] <<	[&]{data_in.write(bit_array<8>(1));} << [&]{write_address.write(bit_array<8>(0));};

		sim[18] <<	[&]{write_enable.write('1');};

		sim[26] <<	[&]{write_enable.write('0');};

		sim[33] <<	[&]{data_in.write(bit_array<8>(2));} << [&]{write_address.write(bit_array<8>(1));};

		sim[38] <<	[&]{write_enable.write('1');};

		sim[46] <<	[&]{write_enable.write('0');};

		sim[53] <<	[&]{data_in.write(bit_array<8>(3));} << [&]{write_address.write(bit_array<8>(2));};

		sim[58] <<	[&]{write_enable.write('1');};

		sim[66] <<	[&]{write_enable.write('0');};

		sim[82] <<	[&]{address_a.write(bit_array<8>(0));};

		sim[92] <<	[&]{address_b.write(bit_array<8>(1));};

		sim[102] <<	[&]{address_c.write(bit_array<8>(2));};

		for (time_type t = 0; t < 200; t += 10) {
			if (t % 20 == 0) 
				sim[t] << [&]{clk.write('1');};
			else
				sim[t] << [&]{clk.write('0');};
		}

		sim.run(0, 205, 1);

		logger.create_log();

		return true;
	}

	util::tester::test_result demo_processor_tester::test_alu() {
		signal<bit_array<3>> func;
		signal<bit_array<8>> op_a;
		signal<bit_array<8>> op_b;

		alu<8> a("alu");

		vcd_event_logger logger("test_data/test_alu.vcd");

		func.connect(a.func);
		op_a.connect(a.op_a);
		op_b.connect(a.op_b);

		func.connect(logger.log_as<bit_array<3>>("func"));
		op_a.connect(logger.log_as<bit_array<8>>("op_a"));
		op_b.connect(logger.log_as<bit_array<8>>("op_b"));
		a.result.connect(logger.log_as<bit_array<8>>("alu_result"));
		a.z_out.connect(logger.log_as<bit_array<1>>("alu_z_out"));
		a.n_out.connect(logger.log_as<bit_array<1>>("alu_n_out"));

		simulation sim;

		sim[0] <<	[&]{func.write(0);} <<
			[&]{op_a.write("01010101");} <<
			[&]{op_b.write("00111001");};

		sim[4] << [&]{func.write(1);};

		sim[8] << [&]{func.write(2);};

		sim[12] << [&]{func.write(3);};

		sim[16] << [&]{func.write(4);};

		sim[22] << [&]{func.write(0);};

		sim[24] << [&]{op_a.write("11111111");} << [&]{op_b.write("00000001");}; 

		sim[28] << [&]{op_a.write("11111111");} << [&]{op_b.write("11111111");}; 

		sim[34] << [&]{func.write(1);};


		sim.run(0, 35, 1);

		logger.create_log();

		return true;
	}

	util::tester::test_result demo_processor_tester::test_instruction_decoder() {
		signal<bit_array<4>> in;

		instruction_decoder a("instruction_decoder");

		vcd_event_logger logger("test_data/test_instruction_decoder.vcd");

		in.connect(a.in);

		in.connect(logger.log_as<bit_array<4>>("in"));
		a.alu_func.connect(logger.log_as<bit_array<3>>("instruction_decoder_alu_func"));

		simulation sim;

		sim[0] << [&]{in.write(0);};

		sim[4] << [&]{in.write(1);};

		sim[8] << [&]{in.write(2);};

		sim[12] << [&]{in.write(3);};

		sim[16] << [&]{in.write(4);};

		sim[22] << [&]{in.write(5);};

		sim[26] << [&]{in.write(6);};

		sim[30] << [&]{in.write(7);};

		sim[34] << [&]{in.write(0);};

		sim.run(0, 35, 1);

		logger.create_log();

		return true;
	}

	util::tester::test_result demo_processor_tester::test_demo_processor() {
		//Memory initializers
		std::array<bit_array<16>, 256> program_initializer = {
			0,
			257,
			767,
			772,
			1026,
			1283,
			7936,
			7681,
			16096,
			36896,
			3328,
			3072,
			2560,
			16368,
			40962,
			32772,
			3329,
			32754,
			16369,
			16096,
			40962,
			32772,
			3073,
			32482,
			16097,
			20478,
			40963,
			15009,
			33021,
			16382,
			18637,
			36867,
			31394,
			15009,
			15824,
			36867,
			32754,
			16369,
			10756,
			12037,
			45056,
			8496,
			33020
		};
		std::fill(program_initializer.begin() + 43, program_initializer.end(), 45056);

		std::array<bit_array<8>, 256> data_initializer = {
			0x85,
			0xfc
		};
		std::fill(data_initializer.begin() + 2, data_initializer.end(), 0);

		//Instantiations
		controller CTRL("controller");
		standard_register<16> IR("ir");
		multiplexer<8, 1> MUX_IP("mux_ip");
		standard_register<8> IP("ip");
		standard_register<4> INSTRUCTION_CODE("instruction_code");
		standard_register<4> DESTINATION("destination");
		standard_register<8> OP_A("op_a");
		standard_register<8> OP_B("op_b");
		standard_register<8> IMM("imm");
		standard_register<8> MAR("mar");
		standard_register<8> MDR("mdr");
		standard_register<1> Z("z");
		standard_register<1> N("n");
		regfile<8, 4> RF("rf");
		multiplexer<8, 1> MUX_IP_ADD("mux_ip_add");
		adder<8> IP_ADD("ip_add");
		adder<8> ADDR_ADD("addr_add");
		instruction_decoder INSTRUCTION_DECODER("instruction_decoder");
		multiplexer<8, 2> MUX_ALU("mux_alu");
		alu<8> ALU("alu");
		regfile<16, 8> INSTRUCTION_CACHE("instruction_cache", program_initializer);	
		regfile<8, 8> DATA_CACHE("data_cache", data_initializer);	
		range_selector<16, 3, 0> RS_IR_3_0("rs_ir_3_0");	
		range_selector<16, 7, 4> RS_IR_7_4("rs_ir_7_4");	
		range_selector<16, 11, 8> RS_IR_11_8("rs_ir_11_8");	
		range_selector<16, 7, 0> RS_IR_7_0("rs_ir_7_0");	
		range_selector<16, 15, 12> RS_IR_15_12("rs_ir_15_12");	

		signal<bit> clock, reset, run;//, clock_neg;

		//Connections
		CTRL.ce_IR.connect(IR.ce);
		CTRL.ce_IP.connect(IP.ce);
		CTRL.ce_destination.connect(DESTINATION.ce);
		CTRL.ce_instruction_code.connect(INSTRUCTION_CODE.ce);
		CTRL.ce_IMM.connect(IMM.ce);
		CTRL.ce_OP_A.connect(OP_A.ce);
		CTRL.ce_OP_B.connect(OP_B.ce);
		CTRL.ce_MAR.connect(MAR.ce);
		CTRL.ce_MDR.connect(MDR.ce);
		CTRL.ce_Z.connect(Z.ce);
		CTRL.ce_N.connect(N.ce);
		CTRL.sel_ALU.connect(MUX_ALU.selector);
		CTRL.sel_IP.connect(MUX_IP.selector);
		CTRL.sel_IP_add.connect(MUX_IP_ADD.selector);
		CTRL.write_data_cache.connect(DATA_CACHE.write_enable);
		CTRL.we_RF.connect(RF.write_enable);
		IR.dout.connect(RS_IR_3_0.in);
		IR.dout.connect(RS_IR_7_4.in);
		IR.dout.connect(RS_IR_11_8.in);
		IR.dout.connect(RS_IR_7_0.in);
		IR.dout.connect(RS_IR_15_12.in);
		RS_IR_7_4.out.connect(RF.address_a);
		RS_IR_3_0.out.connect(RF.address_b);
		RS_IR_11_8.out.connect(RF.address_c);
		RS_IR_11_8.out.connect(DESTINATION.din);
		RS_IR_7_0.out.connect(IMM.din);
		RS_IR_15_12.out.connect(INSTRUCTION_CODE.din);
		RF.data_out_a.connect(OP_A.din);
		RF.data_out_b.connect(OP_B.din);
		RF.data_out_a.connect(ADDR_ADD.in1);
		RF.data_out_b.connect(ADDR_ADD.in2);
		RF.data_out_c.connect(MDR.din);
		ADDR_ADD.result.connect(MAR.din);
		IMM.dout.connect(*(MUX_ALU.inputs[0]));
		DATA_CACHE.data_out_a.connect(*(MUX_ALU.inputs[1]));
		OP_B.dout.connect(*(MUX_ALU.inputs[2]));
		MUX_ALU.out.connect(ALU.op_b);
		OP_A.dout.connect(ALU.op_a);
		DESTINATION.dout.connect(RF.write_address);
		ALU.result.connect(RF.data_in);
		ALU.z_out.connect(Z.din);
		ALU.n_out.connect(N.din);
		Z.dout.connect(CTRL.ss_Z);
		N.dout.connect(CTRL.ss_N);
		INSTRUCTION_CODE.dout.connect(INSTRUCTION_DECODER.in);
		INSTRUCTION_CODE.dout.connect(CTRL.ss_instruction_code);
		INSTRUCTION_DECODER.alu_func.connect(ALU.func);
		MAR.dout.connect(DATA_CACHE.write_address);
		MAR.dout.connect(DATA_CACHE.address_a);
		MDR.dout.connect(DATA_CACHE.data_in);
		INSTRUCTION_CACHE.data_out_a.connect(IR.din);
		IMM.dout.connect(*(MUX_IP_ADD.inputs[1]));
		MUX_IP_ADD.out.connect(IP_ADD.in1);
		IP.dout.connect(IP_ADD.in2);
		IP.dout.connect(INSTRUCTION_CACHE.address_a);
		IP_ADD.result.connect(*(MUX_IP.inputs[1]));
		MUX_IP.out.connect(IP.din);

		clock.connect(IR.clk);
		clock.connect(IP.clk);
		clock.connect(DESTINATION.clk);
		clock.connect(RF.clk);
		clock.connect(CTRL.clk);
		clock.connect(INSTRUCTION_CODE.clk);
		clock.connect(IMM.clk);
		clock.connect(OP_A.clk);
		clock.connect(OP_B.clk);
		clock.connect(MAR.clk);
		clock.connect(MDR.clk);
		clock.connect(Z.clk);
		clock.connect(N.clk);
		clock.connect(INSTRUCTION_CACHE.clk);
		clock.connect(DATA_CACHE.clk);

		reset.connect(IR.reset);
		reset.connect(IP.reset);
		reset.connect(DESTINATION.reset);
		reset.connect(CTRL.reset);
		reset.connect(INSTRUCTION_CODE.reset);
		reset.connect(IMM.reset);
		reset.connect(OP_A.reset);
		reset.connect(OP_B.reset);
		reset.connect(MAR.reset);
		reset.connect(MDR.reset);
		reset.connect(Z.reset);
		reset.connect(N.reset);

		run.connect(CTRL.run);

		//Constant values on multiplexer inputs
		signal<bit_array<8>> constant_one_8;
		signal<bit_array<8>> constant_zero_8;
		constant_one_8.connect(*(MUX_IP_ADD.inputs[0]));
		constant_zero_8.connect(*(MUX_IP.inputs[0]));

		//Connecting signals to the logger
		vcd_event_logger logger("test_data/test_demo_processor.vcd");

		CTRL.end_of_job.connect(logger.log_as<bit>("end_of_job"));
		CTRL.write_data_cache.connect(logger.log_as<bit>("write_data_cache"));
		CTRL.ce_IR.connect(logger.log_as<bit>("ce_IR"));
		CTRL.ce_IP.connect(logger.log_as<bit>("ce_IP"));
		CTRL.ce_instruction_code.connect(logger.log_as<bit>("ce_instruction_code"));
		CTRL.ce_destination.connect(logger.log_as<bit>("ce_destination"));
		CTRL.ce_OP_A.connect(logger.log_as<bit>("ce_OP_A"));
		CTRL.ce_OP_B.connect(logger.log_as<bit>("ce_OP_B"));
		CTRL.ce_IMM.connect(logger.log_as<bit>("ce_IMM"));
		CTRL.ce_MAR.connect(logger.log_as<bit>("ce_MAR"));
		CTRL.ce_MDR.connect(logger.log_as<bit>("ce_MDR"));
		CTRL.ce_Z.connect(logger.log_as<bit>("ce_Z"));
		CTRL.ce_N.connect(logger.log_as<bit>("ce_N"));
		CTRL.we_RF.connect(logger.log_as<bit>("we_RF"));
		CTRL.sel_IP.connect(logger.log_as<bit_array<1>>("sel_IP"));
		CTRL.sel_IP_add.connect(logger.log_as<bit_array<1>>("sel_IP_add"));
		CTRL.sel_ALU.connect(logger.log_as<bit_array<2>>("sel_ALU"));
		IR.dout.connect(logger.log_as<bit_array<16>>("IR.dout"));
		MUX_IP.out.connect(logger.log_as<bit_array<8>>("MUX_IP.out"));
		IP.dout.connect(logger.log_as<bit_array<8>>("IP.dout"));
		INSTRUCTION_CODE.dout.connect(logger.log_as<bit_array<4>>("INSTRUCTION_CODE.dout"));
		DESTINATION.dout.connect(logger.log_as<bit_array<4>>("DESTINATION.dout"));
		OP_A.dout.connect(logger.log_as<bit_array<8>>("OP_A.dout"));
		OP_B.dout.connect(logger.log_as<bit_array<8>>("OP_B.dout"));
		IMM.dout.connect(logger.log_as<bit_array<8>>("IMM.dout"));
		MAR.dout.connect(logger.log_as<bit_array<8>>("MAR.dout"));
		MDR.dout.connect(logger.log_as<bit_array<8>>("MDR.dout"));
		Z.dout.connect(logger.log_as<bit_array<1>>("Z.dout"));
		N.dout.connect(logger.log_as<bit_array<1>>("N.dout"));
		RF.data_out_a.connect(logger.log_as<bit_array<8>>("RF_data_a"));
		RF.data_out_b.connect(logger.log_as<bit_array<8>>("RF_data_b"));
		RF.data_out_c.connect(logger.log_as<bit_array<8>>("RF_data_c"));
		MUX_IP_ADD.out.connect(logger.log_as<bit_array<8>>("MUX_IP_ADD.out"));
		IP_ADD.result.connect(logger.log_as<bit_array<8>>("IP_ADD.result"));
		ADDR_ADD.result.connect(logger.log_as<bit_array<8>>("ADDR_ADD.result"));
		INSTRUCTION_DECODER.alu_func.connect(logger.log_as<bit_array<3>>("INSTUCTION_DECODER.alu_func"));
		MUX_ALU.out.connect(logger.log_as<bit_array<8>>("MUX.out"));
		ALU.result.connect(logger.log_as<bit_array<8>>("ALU.result"));
		INSTRUCTION_CACHE.data_out_a.connect(logger.log_as<bit_array<16>>("INSTRUCTION_CACHE.data_a"));
		INSTRUCTION_CACHE.data_out_b.connect(logger.log_as<bit_array<16>>("INSTRUCTION_CACHE.data_b"));
		INSTRUCTION_CACHE.data_out_c.connect(logger.log_as<bit_array<16>>("INSTRUCTION_CACHE.data_c"));
		DATA_CACHE.data_out_a.connect(logger.log_as<bit_array<8>>("DATA_CACHE.data_a"));
		DATA_CACHE.data_out_b.connect(logger.log_as<bit_array<8>>("DATA_CACHE.data_b"));
		DATA_CACHE.data_out_c.connect(logger.log_as<bit_array<8>>("DATA_CACHE.data_c"));
		RS_IR_3_0.out.connect(logger.log_as<bit_array<4>>("RS_IR_3_0.out"));	
		RS_IR_7_4.out.connect(logger.log_as<bit_array<4>>("RS_IR_7_4.out"));	
		RS_IR_11_8.out.connect(logger.log_as<bit_array<4>>("RS_IR_11_8.out"));	
		RS_IR_7_0.out.connect(logger.log_as<bit_array<8>>("RS_IR_7_0.out"));	
		RS_IR_15_12.out.connect(logger.log_as<bit_array<4>>("RS_IR_15_12.out"));	

		clock.connect(logger.log_as<bit>("clock"));
		reset.connect(logger.log_as<bit>("reset"));
		run.connect(logger.log_as<bit>("run"));
		//clock_neg.connect(logger.log_as<bit>("clock_neg"));

		//Generating input signals
		simulation sim;
		time_type end_of_simulation = 25000;

		sim[0]	<< [&]{constant_one_8.write("00000001");}; 
		sim[0]	<< [&]{constant_zero_8.write("00000000");};
		sim[0]	<< [&]{reset.write('0');};
		sim[0]	<< [&]{run.write('0');};

		sim[12] << [&]{reset.write('1');}; 

		sim[23] << [&]{reset.write('0');}; 

		sim[32] << [&]{run.write('1');}; 

		sim[93] << [&]{run.write('0');}; 

		for (time_type t = 0; t < end_of_simulation; t += 10) {
			if (t % 20 == 0) sim[t] << [&]{clock.write('1');};
			else sim[t] << [&]{clock.write('0');};
		}

		sim.run(0, end_of_simulation, 1);

		logger.create_log();

		return true;
	}
}
