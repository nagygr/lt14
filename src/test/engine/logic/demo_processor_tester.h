/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef TEST_ENGINE_LOGIC_DEMOPROCESSORTESTER
#define TEST_ENGINE_LOGIC_DEMOPROCESSORTESTER

#include <util/tester.h>

namespace test {
	class demo_processor_tester : public util::tester {
		public:
			demo_processor_tester() {
				add_test(__FILE__, &demo_processor_tester::test_flipflop);
				add_test(__FILE__, &demo_processor_tester::test_adder);
				add_test(__FILE__, &demo_processor_tester::test_range_selector);
				add_test(__FILE__, &demo_processor_tester::test_multiplexer);
				add_test(__FILE__, &demo_processor_tester::test_regfile);
				add_test(__FILE__, &demo_processor_tester::test_alu);
				add_test(__FILE__, &demo_processor_tester::test_instruction_decoder);
				add_test(__FILE__, &demo_processor_tester::test_demo_processor);
			}

			static util::tester::test_result test_flipflop();
			static util::tester::test_result test_adder();
			static util::tester::test_result test_range_selector();
			static util::tester::test_result test_multiplexer();
			static util::tester::test_result test_regfile();
			static util::tester::test_result test_alu();
			static util::tester::test_result test_instruction_decoder();
			static util::tester::test_result test_demo_processor();
	};

}

#endif //TEST_ENGINE_LOGIC_DEMOPROCESSORTESTER

