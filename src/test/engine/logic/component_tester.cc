/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <functional>

#include <engine/logic/bit.h>
#include <quantity/time_type.h>
#include <engine/logic/signal.h>
#include <engine/logic/component.h>
#include <engine/logic/bit_array.h>
#include <engine/logic/simulation.h>
#include <engine/logic/output_port.h>
#include <engine/logic/delta_input_port.h>
#include <engine/logic/vcd_event_logger.h>
#include <engine/logic/ostream_event_logger.h>
#include <engine/logic/sensitive_input_port.h>

#include <test/engine/logic/component_tester.h>

namespace test {
	using namespace quantity;
	using namespace engine::logic;

	class counter : public component {
		private:
			unsigned value_;

		public:
			sensitive_input_port<bool> clk;
			delta_input_port<bool> reset;
			output_port<unsigned> output;

		public:
			counter(std::string const &name) : component(name), clk(std::bind(&counter::operation, this)), output(1) {
				ports() << clk << reset << output;
			}

			void operation() {
				if (reset.read()) {
					value_ = 0;
				}
				else if (clk.read()) {
					++value_;
				}

				output.write(value_);
			}
	};

	class nand : public component {
		public:
			sensitive_input_port<bool> a;
			sensitive_input_port<bool> b;
			output_port<bool> output;

		public:
			nand(std::string const &name) : 
				component(name),
				a(std::bind(&nand::operation, this)),
				b(std::bind(&nand::operation, this)),
				output(1) {
				
				ports() << a << b << output;
			}

			void operation() {
				output.write(!(a.read() && b.read()));
			}
	};

	class dff : public component {
		public:
			sensitive_input_port<bool> clk;
			delta_input_port<bool> d;
			output_port<bool> output;

		public:
			dff(std::string const &name) : 
				component(name),
				clk(std::bind(&dff::operation, this)),
				output(1) {
				
				ports() << clk << d << output;
			}

			void operation() {
				if (clk.read()) {
					output.write(d.read());
				}
			}
	};

	class dff_bit : public component {
		public:
			sensitive_input_port<bit> clk;
			delta_input_port<bit> d;
			output_port<bit> output;

		public:
			dff_bit(std::string const &name) : 
				component(name),
				clk(std::bind(&dff_bit::operation, this)),
				output(1) {
				
				ports() << clk << d << output;
			}

			void operation() {
				if (clk.read() == bit::value::high) {
					output.write(d.read());
				}
			}
	};

	class counter_bit_array : public component {
		public:
			sensitive_input_port<bit> clk;
			delta_input_port<bit> reset;
			output_port<bit_array<8>> output;

		public:
			counter_bit_array(std::string const &name) : component(name), clk(std::bind(&counter_bit_array::operation, this)), output(1) {
				ports() << clk << reset << output;
			}

			void operation() {
				if (clk.read() == '1') {
					if (reset.read() == '1') {
						output.write(0);
					}
					else output.write(output.read() + bit_array<8>(1));
				}
			}
	};

	util::tester::test_result component_tester::test_counter() {
		signal<bool> clock;
		signal<bool> reset;

		counter cnt("Counter");

		//ostream_event_logger logger(std::cout);
		vcd_event_logger logger("test_data/test_counter.vcd");

		clock.connect(cnt.clk);
		reset.connect(cnt.reset);

		clock.connect(logger.log_as<bool>("clock"));
		reset.connect(logger.log_as<bool>("reset"));
		cnt.output.connect(logger.log_as<unsigned>("counter"));

		simulation sim;

		sim[0]	<< [&]{reset.write(0);} 
				<< [&]{clock.write(0);};

		sim[1]	<< [&]{reset.write(1);}
				<< [&]{clock.write(0);};

		sim[2]	<< [&]{clock.write(1);};

		sim[3]	<< [&]{reset.write(0);}
				<< [&]{clock.write(0);};

		for (size_t i = 2; i < 5; ++i) {
			sim[2 * i]		<< [&]{clock.write(1);};
			sim[2 * i + 1]	<< [&]{clock.write(0);};
		}

		sim.run(0, 12, 1);

		logger.create_log();

		return true;
	}


	util::tester::test_result component_tester::test_combinational() {
		signal<bool> a_in;
		signal<bool> b_in;

		nand n("NAND");

		//ostream_event_logger logger(std::cout);
		vcd_event_logger logger("test_data/test_nand.vcd");

		a_in.connect(n.a);
		b_in.connect(n.b);
		
		a_in.connect(logger.log_as<bool>("A"));
		b_in.connect(logger.log_as<bool>("B"));
		n.output.connect(logger.log_as<bool>("NAND"));

		simulation sim;

		sim[0]	<< [&]{a_in.write(0);} 
				<< [&]{b_in.write(0);};

		sim[2]	<< [&]{a_in.write(1);} 
				<< [&]{b_in.write(0);};

		sim[4]	<< [&]{a_in.write(1);} 
				<< [&]{b_in.write(1);};

		sim[6]	<< [&]{a_in.write(0);} 
				<< [&]{b_in.write(1);};

		sim[8]	<< [&]{a_in.write(0);} 
				<< [&]{b_in.write(0);};

		sim.run(0, 12, 1);

		logger.create_log();

		return true;
	}

	util::tester::test_result component_tester::test_dff() {
		signal<bool> clock;
		signal<bool> data;

		dff d("DFF");

		vcd_event_logger logger("test_data/test_dff.vcd");

		clock.connect(d.clk);
		data.connect(d.d);
		
		clock.connect(logger.log_as<bool>("CLK"));
		data.connect(logger.log_as<bool>("DATA"));
		d.output.connect(logger.log_as<bool>("DFF"));

		simulation sim;

		sim[1]		<< [&]{data.write(0);}; 

		sim[15]		<< [&]{data.write(1);}; 
		
		sim[20]		<< [&]{data.write(0);}; 
		
		sim[40]		<< [&]{data.write(0);}; 
		
		sim[60]		<< [&]{data.write(1);}; 
		
		sim[125]	<< [&]{data.write(0);}; 

		sim[145]	<< [&]{data.write(1);}; 

		for (time_type t = 0; t < 200; t += 10) {
			if (t % 20 == 0) sim[t] << [&]{clock.write(1);};
			else sim[t] << [&]{clock.write(0);};
		}

		sim.run(0, 210, 1);

		logger.create_log();

		return true;
	}

	util::tester::test_result component_tester::test_delta() {
		signal<bool> clock;
		signal<bool> data;

		dff dff_1("dff_1"), dff_2("dff_2");

		vcd_event_logger logger("test_data/test_delta.vcd");

		clock.connect(dff_1.clk);
		clock.connect(dff_2.clk);
		data.connect(dff_1.d);
		dff_1.output.connect(dff_2.d);
		
		clock.connect(logger.log_as<bool>("clock"));
		data.connect(logger.log_as<bool>("data"));
		dff_1.output.connect(logger.log_as<bool>("dff_1"));
		dff_2.output.connect(logger.log_as<bool>("dff_2"));

		simulation sim;

		for (time_type t = 5; t < 200; t += 20) {
			if ((t - 5) % 40 == 0) sim[t] << [&]{data.write(1);};
			else sim[t] << [&]{data.write(0);};
		}

		for (time_type t = 0; t < 350; t += 10) {
			if (t % 20 == 0) sim[t] << [&]{clock.write(1);};
			else sim[t] << [&]{clock.write(0);};
		}

		sim.run(0, 360, 1);

		logger.create_log();

		return true;
	}

	util::tester::test_result component_tester::test_dff_bit() {
		signal<bit> clock;
		signal<bit> data;

		dff_bit d("DFF");

		vcd_event_logger logger("test_data/test_dff_bit.vcd");

		clock.connect(d.clk);
		data.connect(d.d);
		
		clock.connect(logger.log_as<bit>("CLK"));
		data.connect(logger.log_as<bit>("DATA"));
		d.output.connect(logger.log_as<bit>("DFF"));

		simulation sim;

		sim[1]		<< [&]{data.write('0');}; 

		sim[15]		<< [&]{data.write('1');}; 
		
		sim[20]		<< [&]{data.write('0');}; 
		
		sim[40]		<< [&]{data.write('0');}; 
		
		sim[60]		<< [&]{data.write('1');}; 
		
		sim[125]	<< [&]{data.write('0');}; 

		sim[145]	<< [&]{data.write('1');}; 

		for (time_type t = 0; t < 200; t += 10) {
			if (t % 20 == 0) sim[t] << [&]{clock.write('1');};
			else sim[t] << [&]{clock.write('0');};
		}

		sim.run(0, 210, 1);

		logger.create_log();

		return true;
	}

	util::tester::test_result component_tester::test_counter_bit_array() {
		signal<bit> clock;
		signal<bit> reset;

		counter_bit_array cnt("Counter");

		vcd_event_logger logger("test_data/test_counter_bit_array.vcd");

		clock.connect(cnt.clk);
		reset.connect(cnt.reset);

		clock.connect(logger.log_as<bit>("clock"));
		reset.connect(logger.log_as<bit>("reset"));
		cnt.output.connect(logger.log_as<bit_array<8>>("counter"));

		simulation sim;

		sim[0]	<< [&]{reset.write('0');}; 

		sim[32] << [&]{reset.write('1');}; 

		sim[43] << [&]{reset.write('0');}; 

		for (time_type t = 0; t < 200; t += 10) {
			if (t % 20 == 0) sim[t] << [&]{clock.write('1');};
			else sim[t] << [&]{clock.write('0');};
		}

		sim.run(0, 210, 1);

		logger.create_log();

		return true;
	}
}
