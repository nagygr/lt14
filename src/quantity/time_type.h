/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef QUANTITY_TIMETYPE
#define QUANTITY_TIMETYPE

#include <string>
#include <utility>
#include <iostream>

namespace quantity {
	/**
	 * Represents time_type in the simulations.
	 */
	class time_type {
		public:
			/**
			 * Represents the time_type units.
			 */
			enum class unit {
				fs, /**< femtoseconds */
				ps, /**< picoseconds */
				ns, /**< nanoseconds */
				us, /**< microseconds */
				ms, /**< milliseconds */
				s     /**< seconds */
			};

			/** Type that represents the value of a time_type-point. */
			using value_type = signed long long int; 

		private:
			value_type the_value_; /**< The value. */
			unit the_unit_; /**< The unit. */

		public:
			/**
			 * Constructor.
			 * @param a_value the value
			 * @param a_unit the unit
			 */
			time_type(value_type a_value = 0, unit a_unit = unit::us) : the_value_(a_value), the_unit_(a_unit) {}

			/**
			 * Constructor.
			 * Creates a time_type object by extracting the unit from a string.
			 * @param a_value the value
			 * @param a_unit the unit
			 */
			time_type(value_type a_value, std::string const &a_unit);

			/**
			 * Gets the value.
			 * @return the value
			 */
			value_type get_value() const {return the_value_;}

			/**
			 * Gets the unit.
			 * @return the unit
			 */
			unit get_unit() const {return the_unit_;}

			/**
			 * Addition assignment operator.
			 * @param rhs the addend
			 * @return reference to the incremented object
			 */
			time_type &operator +=(time_type const &rhs);

			/**
			 * Addition assignment operator with value_type.
			 * @param rhs the addend
			 * @return reference to the incremented object
			 */
			time_type &operator +=(value_type const &rhs);

			/**
			 * Subtraction assignment operator.
			 * @param rhs the subtrahend
			 * @return reference to the decremented object
			 */
			time_type &operator -=(time_type const &rhs);

			/**
			 * Subtraction assignment operator with value_type.
			 * @param rhs the subtrahend
			 * @return reference to the incremented object
			 */
			time_type &operator -=(value_type const &rhs);

			/**
			 * Modulo operator.
			 * @param rhs the divisor
			 * @return the modulo of the division
			 */
			time_type &operator %=(time_type const &rhs);

			/**
			 * Modulo assignment operator with value_type.
			 * @param rhs the divisor
			 * @return reference to the incremented object
			 */
			time_type &operator %=(value_type const &rhs);
			
			/**
			 * Yields time-point in seconds as a double.
			 * @return time-point in seconds as a double
			 */
			double in_seconds() const;

			/**
			 * Returns a pair of time_type the unit of which set to the smaller of the input time_type instances.
			 * @param t1 a time_type object
			 * @param t2 a time_type object
			 * @return the two time_type values at the smaller unit of the two inputs
			 */
			static std::pair<time_type, time_type> match_unit(time_type const &t1, time_type const &t2);
	};

	/**
	 * Addition operator.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 */
	inline time_type operator +(time_type lhs, time_type const &rhs) {return lhs += rhs;}

	/**
	 * Addition operator for value_type.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 */
	inline time_type operator +(time_type lhs, time_type::value_type const &rhs) {return lhs += rhs;}

	/**
	 * Subtraction operator.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 */
	inline time_type operator -(time_type lhs, time_type const &rhs) {return lhs -= rhs;}

	/**
	 * Subtraction operator for value_type.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 */
	inline time_type operator -(time_type lhs, time_type::value_type const &rhs) {return lhs -= rhs;}

	/**
	 * Modulo operator.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 */
	inline time_type operator %(time_type lhs, time_type const &rhs) {return lhs %= rhs;}

	/**
	 * Modulo operator for value_type.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 */
	inline time_type operator %(time_type lhs, time_type::value_type const &rhs) {return lhs %= rhs;}

	/**
	 * Equal to operator.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 * @return true if the two time_type objects are equal
	 */
	bool operator ==(time_type const &lhs, time_type const &rhs);

	/**
	 * Equal to operator for value_type.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 * @return true if the two time_type objects are equal
	 */
	inline bool operator ==(time_type const &lhs, time_type::value_type const &rhs) {return lhs.get_value() == rhs;}

	/**
	 * Not equal to operator.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 * @return true if the two time_type objects are not equal
	 */
	inline bool operator !=(time_type const &lhs, time_type const &rhs) {return !(lhs == rhs);}

	/**
	 * Not equal to operator for time_type::value_type.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 * @return true if the two time_type objects are not equal
	 */
	inline bool operator !=(time_type const &lhs, time_type::value_type const &rhs) {return !(lhs == rhs);}

	/**
	 * Less operator.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 * @return true if the first time_type-point is earlier than the second
	 */
	bool operator <(time_type const &lhs, time_type const &rhs);

	/**
	 * Less operator for time_type::value_type.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 * @return true if the first time_type-point is earlier than the second
	 */
	inline bool operator <(time_type const &lhs, time_type::value_type const &rhs) {return lhs.get_value() < rhs;}

	/**
	 * Greater operator.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 * @return true if the first time_type-point is later than the second
	 */
	inline bool operator >(time_type const &lhs, time_type const &rhs) {return !(lhs < rhs || lhs == rhs);}

	/**
	 * Greater operator for time_type::value_type.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 * @return true if the first time_type-point is later than the second
	 */
	inline bool operator >(time_type const &lhs, time_type::value_type const &rhs) {return !(lhs < rhs || lhs == rhs);}

	/**
	 * Less or equal operator.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 * @return true if the first time_type-point is not later than the second
	 */
	inline bool operator <=(time_type const &lhs, time_type const &rhs) {return !(lhs > rhs);}

	/**
	 * Less or equal operator for time_type::value_type.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 * @return true if the first time_type-point is not later than the second
	 */
	inline bool operator <=(time_type const &lhs, time_type::value_type const &rhs) {return !(lhs > rhs);}

	/**
	 * Greater or equal operator.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 * @return true if the first time_type-point is not earlier than the second
	 */
	inline bool operator >=(time_type const &lhs, time_type const &rhs) {return !(lhs < rhs);}

	/**
	 * Greater or equal operator for time_type::value_type.
	 * @param lhs the left hand side operator
	 * @param rhs the right hand side operator
	 * @return true if the first time_type-point is not earlier than the second
	 */
	inline bool operator >=(time_type const &lhs, time_type::value_type const &rhs) {return !(lhs < rhs);}

	/**
	 * Puts a time_type object to an output stream
	 * @param os the output stream
	 * @param t the time_type object
	 * @return a reference to the output stream
	 */
	std::ostream &operator<<(std::ostream &os, time_type const &t);
}

namespace std {
	/**
	 * std::hash specialization for \ref quantity::time_type
	 */
	template <>
	struct hash<quantity::time_type> {
		public:
			size_t operator()(quantity::time_type const &time_point) const {
				return hash<quantity::time_type::value_type>()(time_point.get_value());
			}
	};
}

#endif // QUANTITY_TIMETYPE
