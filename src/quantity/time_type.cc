/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <cmath>
#include <sstream>
#include <cstddef>
#include <stdexcept>

#include <quantity/time_type.h>

namespace quantity {
	time_type::time_type(value_type a_value, std::string const &a_unit) : the_value_(a_value) {
		if (a_unit == "fs") the_unit_ = unit::fs;
		else if (a_unit == "ps") the_unit_ = unit::ps;
		else if (a_unit == "ns") the_unit_ = unit::ns;
		else if (a_unit == "us") the_unit_ = unit::us;
		else if (a_unit == "ms") the_unit_ = unit::ms;
		else if (a_unit == "s") the_unit_ = unit::s;
		else {
			std::stringstream error;
			error << "Invalid argument to time_type::time_type constructor as a unit: " << a_unit;
			throw std::invalid_argument(error.str());
		}
	}

	time_type &time_type::operator +=(time_type const &rhs) {
		auto matched_values = match_unit(*this, rhs);
		the_value_ = matched_values.first.the_value_ + matched_values.second.the_value_;
		the_unit_ = matched_values.first.the_unit_;

		return *this;
	}

	time_type &time_type::operator +=(value_type const &rhs) {
		the_value_ += rhs;
		return *this;
	}

	time_type &time_type::operator -=(time_type const &rhs) {
		auto matched_values = match_unit(*this, rhs);
		the_value_ = matched_values.first.the_value_ - matched_values.second.the_value_;
		the_unit_ = matched_values.first.the_unit_;

		return *this;
	}

	time_type &time_type::operator -=(value_type const &rhs) {
		the_value_ -= rhs;
		return *this;
	}

	time_type &time_type::operator %=(time_type const &rhs) {
		auto matched_values = match_unit(*this, rhs);
		
		the_value_ = matched_values.first.the_value_ % matched_values.second.the_value_;
		the_unit_ = matched_values.first.the_unit_;

		return *this;
	}

	time_type &time_type::operator %=(value_type const &rhs) {
		the_value_ %= rhs;
		return *this;
	}

	bool operator ==(time_type const &lhs, time_type const &rhs) {
		auto matched_values = time_type::match_unit(lhs, rhs);
		return matched_values.first.get_value() == matched_values.second.get_value();
	}
	
	bool operator <(time_type const &lhs, time_type const &rhs) {
		auto matched_values = time_type::match_unit(lhs, rhs);
		return matched_values.first.get_value() < matched_values.second.get_value();
	}	

	std::pair<time_type, time_type> time_type::match_unit(time_type const &t1, time_type const &t2) {
		size_t unit1 = static_cast<size_t>(t1.the_unit_);
		size_t unit2 = static_cast<size_t>(t2.the_unit_);
		value_type value1 = t1.the_value_;
		value_type value2 = t2.the_value_;

		unit new_unit;

		if (unit1 < unit2) {
			value2 *= std::pow(10, 3 * (unit2 - unit1)); 
			new_unit = t1.the_unit_;
		}
		else if (unit2 < unit1) {
			value1 *= std::pow(10, 3 * (unit1 - unit2));
			new_unit = t2.the_unit_;
		}
		else {
			new_unit = t1.the_unit_; // == t2.the_unit_
		}

		return std::make_pair(time_type(value1, new_unit), time_type(value2, new_unit));
	}

	std::ostream &operator<<(std::ostream &os, time_type const &t) {
		os << t.get_value();

		switch (t.get_unit()) {
			case time_type::unit::fs:
				os << "fs";
				break;
			case time_type::unit::ps:
				os << "ps";
				break;
			case time_type::unit::ns:
				os << "ns";
				break;
			case time_type::unit::us:
				os << "us";
				break;
			case time_type::unit::ms:
				os << "ms";
				break;
			case time_type::unit::s:
				os << "s";
				break;
		}

		return os;
	}

	double time_type::in_seconds() const {
		double exponent;

		switch (the_unit_) {
			case time_type::unit::fs:
				exponent = 1e-15;
				break;
			case time_type::unit::ps:
				exponent = 1e-12;
				break;
			case time_type::unit::ns:
				exponent = 1e-9;
				break;
			case time_type::unit::us:
				exponent = 1e-6;
				break;
			case time_type::unit::ms:
				exponent = 1e-3;
				break;
			case time_type::unit::s:
			default:
				exponent = 1.0;
				break;
		}

		return the_value_ * exponent;
	}
}
