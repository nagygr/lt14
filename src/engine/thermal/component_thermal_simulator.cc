/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <algorithm>
#include <iostream>

#include <engine/thermal/component_thermal_simulator.h>

namespace engine {
	namespace thermal {
		component_thermal_simulator::component_thermal_simulator(std::vector<engine::logic::component *> &components, std::vector<std::tuple<std::string,std::shared_ptr<layout_information>>> &layout) {
			for (auto &layout_element: layout) {
				auto component = std::find_if(components.begin(), components.end(), [&](engine::logic::component *c){return c->get_name() == std::get<0>(layout_element);});

				if (component != components.end()) {
					components_.push_back(std::make_tuple(*component, std::get<1>(layout_element)));	
				}
			}
		}
		
		void component_thermal_simulator::refresh_temperatures(quantity::time_type const &now) {
			start_new_simulation_cycle(now);

			for (auto &component: components_) {
				engine::logic::component *a_component = std::get<0>(component);
				if (a_component->was_active()) {
					add_dissipation(component, a_component->get_dissipation());
				}
			}

			calculate_temperature_map();
			
			for (auto &component: components_) {
				refresh_component_temperature(component);
			}
		}
	}
}
