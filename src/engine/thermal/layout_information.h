/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_THERMAL_LAYOUTINFORMATION
#define ENGINE_THERMAL_LAYOUTINFORMATION

#include <tuple>

namespace engine {
	namespace thermal {
		/**
		 * Generic interface for the classes that contain information about the physical layout of a component.
		 */
		class layout_information {
			public:
				using coordinates = std::tuple<size_t, size_t>; /**< Type representing 2D coordinates in a layout. */

			public:
				/**
				 * Destructor.
				 */
				virtual ~layout_information() {}

				/**
				 * Gets the x and y coordinates of a component.
				 * @return the x and y coordinates of a component 
				 */
				virtual coordinates get_xy() const = 0;
		};
	}
}

#endif // ENGINE_THERMAL_LAYOUTINFORMATION

