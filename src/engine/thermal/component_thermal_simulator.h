/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_THERMAL_COMPONENTTHERMALSIMULATOR
#define ENGINE_THERMAL_COMPONENTTHERMALSIMULATOR

#include <tuple>
#include <vector>
#include <memory>
#include <string>

#include <engine/logic/component.h>
#include <engine/thermal/thermal_simulator.h>
#include <engine/thermal/layout_information.h>

namespace engine {
	namespace thermal {
		/**
		 * Class representing thermal simulators based on \ref engine::logic::component instances.
		 */
		class component_thermal_simulator : public thermal_simulator {
			protected:
				/**
				 * Type representing the set of information on a component.
				 */
				using component_information = std::tuple<engine::logic::component *, std::shared_ptr<layout_information>>;

			private:
				std::vector<component_information> components_; /**< The container holding the components. */

			public:
				/**
				 * Constructor.
				 * Combines the information gathered from the two containers received as parameters and fills the \ref components_ container with the information
				 * that links the logic components with their representation in the layout. This makes it possible to access this information very effectively.
				 * @param components the container holding the logic components
				 * @param layout the container holding the layout
				 */
				component_thermal_simulator(std::vector<engine::logic::component *> &components, std::vector<std::tuple<std::string,std::shared_ptr<layout_information>>> &layout);
				
				/**
				 * @copydoc engine::thermal::thermal_simulator::refresh_temperatures
				 */
				virtual void refresh_temperatures(quantity::time_type const &now) override;

			protected:
				/**
				 * Starts a new simulation cycle in the thermal simulator engine.
				 * This method can be used for example to clear all dissipation data in the
				 * representation of the thermal structure before filling it with
				 * the current dissipation values obtained by \ref add_dissipation calls.
				 * The now parameter can be used to determine the elapsed time 
				 * between the previous and the current simulation cycles.
				 * @param now the current time-point in the simulation
				 */
				virtual void start_new_simulation_cycle(quantity::time_type const &now) = 0;

				/**
				 * Adds a dissipation value in the current simulation cycle.
				 * @param a_component a component
				 * @param dissipation the current dissipation of the component
				 */
				virtual void add_dissipation(component_information &a_component, double dissipation) = 0;

				/**
				 * Calculates the new temperature map based on the latest dissipation values. 
				 */
				virtual void calculate_temperature_map() = 0;
				
				/**
				 * Refreshes a component's temperature.
				 * The information that connects the thermal simulator's inner
				 * representation of temperatures and the temperature data
				 * member of the components should be placed in this function.
				 * @param a_component a component
				 */
				virtual void refresh_component_temperature(component_information &a_component) = 0; 

				/**
				 * Gets the container holding components.
				 * @return the container holding components
				 */
				std::vector<component_information> &get_components() {return components_;}
		};
	}
}

#endif // ENGINE_THERMAL_COMPONENTTHERMALSIMULATOR

