/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <tuple>
#include <string>
#include <memory>
#include <utility>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <algorithm>

#include <util/math/vector.h>
#include <engine/thermal/equivalent_circuit/resistor.h>
#include <engine/thermal/equivalent_circuit/capacitor.h>

#include <engine/thermal/equivalent_circuit/thermal_simulator.h>
#include <engine/thermal/equivalent_circuit/layout_information.h>

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
			thermal_simulator::thermal_simulator(
					std::vector<engine::logic::component *> &components,
					std::vector<std::tuple<std::string,std::shared_ptr<engine::thermal::layout_information>>> &layout,
					engine::thermal::layout_information::coordinates const &layout_size,
					double grid_size_x,
					double grid_size_y,
					double wafer_thickness,
					quantity::time_type time_step
					) :
				engine::thermal::component_thermal_simulator(components, layout),
				layout_size_(layout_size),
				grid_size_x_(grid_size_x),
				grid_size_y_(grid_size_y),
				wafer_thickness_(wafer_thickness),
				circuit_(std::get<0>(layout_size) * std::get<1>(layout_size) + 1), // +1: ground node
				time_step_(time_step)
			{
				constexpr double lambda = 148;	// specific heat conductivity
				constexpr double rho = 2330;	// density
				constexpr double c = 703;		// specific heat capacity

				double const heat_capacitance = c * rho * grid_size_x * grid_size_y * wafer_thickness; // heat capacitance of a cuboid (one grid cell with a thickness equal to the wafer thickness
				double const area_x = grid_size_y * wafer_thickness; // The area of the cuboid's horizontal faces
				double const area_y = grid_size_x * wafer_thickness; // The area of the cuboid's horizontal faces
				double const area_z = grid_size_x * grid_size_y; // The area of the cuboid's top and bottom faces
				double const r_x = grid_size_x / (lambda * area_x); // The horizontal thermal resistance of the cuboid 
				double const r_y = grid_size_y / (lambda * area_y); // The vertical thermal resistance of the cuboid 
				double const r_z = wafer_thickness / (lambda * area_z); // The thermal resistance of the cuboid towards the thermal ground

				size_t const size_x = std::get<0>(layout_size);
				size_t const size_y = std::get<1>(layout_size);

				for (size_t y = 0; y < size_y; ++y) {
					for (size_t x = 0; x < size_x; ++x) {
						size_t pos = (y * size_x) + x + 1; //+1 due to ground node

						if (x < size_x - 1) resistor(pos, pos + 1, r_x).add_to(circuit_);
						if (y < size_y - 1) resistor(pos, pos + size_x, r_y).add_to(circuit_);

						capacitor(pos, 0, heat_capacitance, time_step_.in_seconds()).add_to(circuit_);
						resistor(pos, 0, r_z).add_to(circuit_);
					}
				}

				for (auto &a_component: get_components()) {
					auto layout = std::dynamic_pointer_cast<engine::thermal::equivalent_circuit::layout_information>(std::get<1>(a_component));
					if (!layout) throw incompatible_layout_information(); 
					layout->add_dissipator_to(circuit_);
				}

			}

			void thermal_simulator::start_new_simulation_cycle(quantity::time_type const &now) {
				for (auto &a_component: get_components()) {
					auto layout = std::dynamic_pointer_cast<engine::thermal::equivalent_circuit::layout_information>(std::get<1>(a_component));
					if (!layout) throw incompatible_layout_information(); 
					layout->set_dissipation(0.0);
				}
			}

			void thermal_simulator::add_dissipation(engine::thermal::component_thermal_simulator::component_information &a_component, double dissipation) {
				auto layout = std::dynamic_pointer_cast<engine::thermal::equivalent_circuit::layout_information>(std::get<1>(a_component));
				if (!layout) throw incompatible_layout_information(); 
				layout->set_dissipation(dissipation);
			}

			void thermal_simulator::calculate_temperature_map() {
				circuit_.solve();
			}

			void thermal_simulator::refresh_component_temperature(engine::thermal::component_thermal_simulator::component_information &a_component) { 
				engine::logic::component &logic_component = *std::get<0>(a_component);
				auto layout = std::get<1>(a_component);

				size_t const x = std::get<0>(layout->get_xy());
				size_t const y = std::get<1>(layout->get_xy());
				size_t const position = (y * std::get<0>(layout_size_)) + x; 

				double temperature = circuit_.get_v()[position];
				logic_component.set_temperature(temperature);
			}

			void thermal_simulator::write_on_stream(std::ostream &stream) {
				struct color {
					unsigned red;
					unsigned green;
					unsigned blue;

					color(unsigned red = 0, unsigned green = 0, unsigned blue = 0) : red(red), green(green), blue(blue) {}

					std::string to_hex() const {
						std::stringstream stream;

						stream << std::setfill('0');
						stream << std::hex << std::setw(2) << red << std::setw(2) << green << std::setw(2) << blue;
						return stream.str();
					}
				};

				constexpr size_t picture_grid_x = 10; // horizontal size of one grid in pixels
				constexpr size_t picture_grid_y = 10; // vertical size of one grid in pixels
				size_t const size_x = std::get<0>(layout_size_);
				size_t const size_y = std::get<1>(layout_size_);

				util::math::vector<double> &temperatures = circuit_.get_v();
				auto min_max = minmax_element(temperatures.begin(), temperatures.end());
				double temperature_range = *min_max.second - *min_max.first;

				unsigned const max_col = 255;

				stream << "<?xml version=\"1.0\" standalone=\"no\"?>" << std::endl;
				stream << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">" << std::endl;
				stream << "<svg width=\"" << (size_x * picture_grid_x) << "\" height=\"" << (size_y * picture_grid_y) << "\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">" << std::endl;

				for (size_t i = 0; i < temperatures.get_size(); ++i) {
					double temperature = temperatures[i];
					double value = (temperature - *min_max.first) / temperature_range;

					size_t position_x = i % size_x;
					size_t position_y = i / size_x;

					unsigned red = ((value < 0.3) ? 0.0 : (1.0 / 0.7) * (value - 0.3)) * max_col;
					unsigned green = (0.5 * value * value * value * value) * max_col;
					unsigned blue = (0.5 * (1 - value * value * value)) * max_col;
					color the_color(red, green, blue);

					stream << "<rect x=\"" << (position_x * picture_grid_x) << "\" y=\"" << (position_y * picture_grid_y) << "\" width=\"" << picture_grid_x << "\" height=\"" << picture_grid_y << "\" style=\"fill:#" << the_color.to_hex() << ";stroke-width:0;\"/>" << std::endl;
				}

				stream << "</svg>" << std::endl;
			}
		}
	}
}

