/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_THERMAL_EQUIVALENTCIRCUIT_LAYOUTINFORMATION
#define ENGINE_THERMAL_EQUIVALENTCIRCUIT_LAYOUTINFORMATION

#include <memory>
#include <exception>
#include <stdexcept>

#include <engine/thermal/equivalent_circuit/current.h>
#include <engine/thermal/equivalent_circuit/linear_circuit.h>

#include <engine/thermal/layout_information.h>

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
		/**
		 * Represents the details of an element's physical layout.
		 */
		class layout_information : public engine::thermal::layout_information {
			public:
				/**
				 * Thrown when set_dissipation is called on an object that has not been added
				 * to a linear_circuit using add_dissipator_to.
				 * This can happen, as the linear_circuit might not be ready at contruction time,
				 * so it can not be a contructor argument.
				 */
				class circuit_not_set : public std::exception {
					public:
						virtual char const *what() const noexcept override {
							return "The linear_circuit has not been set in an equivalent_circuit_layout_information object.";
						} 
				};

			private:
				/**
				 * The grids in the layout in two dimensions.
				 */
				engine::thermal::layout_information::coordinates layout_size_;

				/**
				 * The coordinates of the dissipator.
				 */
				engine::thermal::layout_information::coordinates xy_coordinates_; 

				/**
				 * The current source corresponding to the dissipator.
				 */
				std::unique_ptr<current> current_;

				/**
				 * The address of the circuit that the dissipator is added to.
				 * @note The object pointed to is not owned by this object.
				 */
				linear_circuit *the_circuit_;

			public:
				/**
				 * Constructor.
				 * @param layout_size the number of grids on the layout in two dimensions
				 * @param xy_coordinates the coordinates of the element on the layout
				 * @throw std::out_of_range if the xy_coordinates are outside the layout defined by layout_size
				 */
				layout_information(engine::thermal::layout_information::coordinates const &layout_size, engine::thermal::layout_information::coordinates const &xy_coordinates) : 
					layout_size_(layout_size),
					xy_coordinates_(xy_coordinates),
					the_circuit_(nullptr) {
						if (std::get<0>(layout_size) <= std::get<0>(xy_coordinates) || std::get<1>(layout_size) <= std::get<1>(xy_coordinates)) throw std::out_of_range("XY coordinates of a layout element are outside the layout");
					} 

				/**
				 * Gets the xy coordinates of the element on the layout.
				 * @return the xy coordinates of the element on the layout
				 */
				virtual engine::thermal::layout_information::coordinates get_xy() const override {return xy_coordinates_;}

				/**
				 * Adds the dissipator to a linear_circuit.
				 * This method also sets the object's \ref the_circuit_ data member so that
				 * the circuit is not needed at every \ref set_dissipation call. This means though
				 * that this method has to be called before the first call to set_dissipation,
				 * otherwise it will throw a \ref circuit_not_set exception.
				 * @param the_circuit the linear_circuit that the dissipator is added to
				 */
				void add_dissipator_to(linear_circuit &the_circuit);

				/**
				 * Changes the dissipation value of the dissipator.
				 * @throw \ref circuit_not_set if the \ref add_dissipator_to method has not been
				 * called on the object (and so \ref the_circuit_ data member has not been set).
				 * @param dissipation the new dissipation of the element
				 */
				void set_dissipation(double dissipation);
		};
	}
	}
}

#endif // ENGINE_THERMAL_EQUIVALENTCIRCUIT_LAYOUTINFORMATION

