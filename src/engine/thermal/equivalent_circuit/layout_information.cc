/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <tuple>

#include <engine/thermal/equivalent_circuit/layout_information.h>

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
			void layout_information::add_dissipator_to(linear_circuit &the_circuit) {
				size_t size_x = std::get<0>(layout_size_);
				size_t x = std::get<0>(xy_coordinates_), y = std::get<0>(xy_coordinates_);
				size_t node = (y * size_x) + x + 1;

				current_ = std::unique_ptr<current>(new current(node, 0, 0.0));
				current_->add_to(the_circuit);

				the_circuit_ = &the_circuit;
			}

			void layout_information::set_dissipation(double dissipation) {
				if (the_circuit_ == nullptr) throw circuit_not_set();

				current_->change_value(*the_circuit_, dissipation);
			}
		}
	}
}
