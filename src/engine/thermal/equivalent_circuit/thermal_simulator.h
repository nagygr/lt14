/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_THERMAL_EQUIVALENTCIRCUIT_THERMALSIMULATOR
#define ENGINE_THERMAL_EQUIVALENTCIRCUIT_THERMALSIMULATOR

#include <iostream>

#include <engine/thermal/equivalent_circuit/linear_circuit.h>

#include <quantity/time_type.h>
#include <engine/thermal/component_thermal_simulator.h>

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
			/**
			 * A thermal simulator that models the thermal behaviour of the circuit with an equivalent circuit.
			 * This is basically a facade class to the ects library linked to the project.
			 */
			class thermal_simulator : public engine::thermal::component_thermal_simulator {
				public:
					/**
					 * Thrown when an incompatible subclass of \ref engine::thermal::layout_information
					 * is found.
					 */
					class incompatible_layout_information : public std::exception {
						public:
							virtual char const *what() const noexcept override {
								return "The equivalent_circuit_thermal_simulator requires equivalent_circuit_layout_information but received a layout_information of a different type.";
							} 
					};
				private:
					engine::thermal::layout_information::coordinates layout_size_;	/**< The size of the layout in grids.				*/
					double grid_size_x_;											/**< The horizontal physical size of a grid.		*/
					double grid_size_y_;											/**< The vertical physical size of a grid.			*/
					double wafer_thickness_;										/**< The thickness of the wafer.					*/
					engine::thermal::equivalent_circuit::linear_circuit circuit_;	/**< The equivalent circuit representing the wafer.	*/

					/**
					 * The time step in the simulation.
					 * @note Needed by the capacitors.
					 */
					quantity::time_type time_step_;

				public:
					/**
					 * Constructor
					 * @param components the logic components in the simulation
					 * @param layout the lalyout information
					 * @param layout_size the number of grids in the layout
					 * @param grid_size_x the vertical physical size of the layout
					 * @param grid_size_y the horizontal physical size of the layout
					 * @param wafer_thickness the thickness of the wafer
					 * @param time_step the time step in the simulation
					 */
					thermal_simulator(
							std::vector<engine::logic::component *> &components,
							std::vector<std::tuple<std::string,std::shared_ptr<layout_information>>> &layout,
							engine::thermal::layout_information::coordinates const &layout_size,
							double grid_size_x,
							double grid_size_y,
							double wafer_thickness,
							quantity::time_type time_step
							);

					/**
					 * @copydoc engine::thermal::thermal_simulator::write_on_stream
					 */
					virtual void write_on_stream(std::ostream &stream) override;

				protected:
					/**
					 * @copydoc engine::thermal::component_thermal_simulator::start_new_simulation_cycle
					 */
					virtual void start_new_simulation_cycle(quantity::time_type const &now) override;

					/**
					 * @copydoc engine::thermal::component_thermal_simulator::add_dissipation
					 */
					virtual void add_dissipation(engine::thermal::component_thermal_simulator::component_information &a_component, double dissipation) override;

					/**
					 * @copydoc engine::thermal::component_thermal_simulator::calculate_temperature_map
					 */
					virtual void calculate_temperature_map() override;

					/**
					 * @copydoc engine::thermal::component_thermal_simulator::refresh_component_temperature
					 */
					virtual void refresh_component_temperature(engine::thermal::component_thermal_simulator::component_information &a_component) override; 
			};
		}
	}
}

#endif // ENGINE_THERMAL_EQUIVALENTCIRCUIT_THERMALSIMULATOR
