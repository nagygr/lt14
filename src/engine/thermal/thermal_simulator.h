/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_THERMAL_THERMALSIMULATOR
#define ENGINE_THERMAL_THERMALSIMULATOR

#include <iostream>

#include <quantity/time_type.h>

namespace engine {
	namespace thermal {
		/**
		 * Generic interface for thermal simulators.
		 * All that a \ref engine::logic::simulation instance should see of a thermal simulator
		 * is the \ref refresh_temperatures method.
		 * The method how the thermal simulator gets to know the components and all the relevant information
		 * has to be defined in the subclasses.
		 */
		class thermal_simulator {
			public:
				/**
				 * Refreshes the component temperatures with new values
				 * calculated based on the activity and dissipation of the
				 * components and the physical layout.
				 * @param now the current time-point in the simulation
				 */
				virtual void refresh_temperatures(quantity::time_type const &now) = 0;

				/**
				 * Writes the results on an output stream.
				 * @stream the stream to write on
				 */
				virtual void write_on_stream(std::ostream &stream) = 0;

				/**
				 * Destructor.
				 */
				virtual ~thermal_simulator() {}
		};
	}
}

#endif // ENGINE_THERMAL_THERMALSIMULATOR
