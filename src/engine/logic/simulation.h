/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_SIMULATION
#define ENGINE_LOGIC_SIMULATION

#include <vector>
#include <functional>
#include <unordered_map>

#include <quantity/time_type.h>
#include <engine/logic/component.h>

#include <engine/thermal/thermal_simulator.h>

namespace engine {
	namespace logic {
		/**
		 * The controller of the simulation flow.
		 */
		class simulation {
			public:
				/**
				 * Helper class that makes storing test vectors easier.
				 */
				class test_injector {
					private:
						std::unordered_map<quantity::time_type, std::vector<std::function<void()>>> &test_vector_; /**< Reference of the container holding the test vectors. */
						quantity::time_type index_; /**< The time-point where new tests are to be inserted. */

					public:
						/**
						 * Constructor.
						 * @param test_vector reference of the container holding the test vectors (the container itself resides in \ref simulation.
						 * @param index the place where the test vectors are inserted
						 */
						test_injector(std::unordered_map<quantity::time_type, std::vector<std::function<void()>>> &test_vector, quantity::time_type index) :
							test_vector_(test_vector),
							index_(index) {}

						/**
						 * The shift left operator is used to insert a test function in the container
						 * @param a_function a test function
						 * @return reference of the \ref test_injector
						 */
						test_injector &operator<<(std::function<void()> a_function) {
							test_vector_[index_].push_back(a_function);
							return *this;
						}
				};

			private:
				std::unordered_map<quantity::time_type, std::vector<std::function<void()>>> test_vector_; /**< The container holding the test vectors. */
				engine::thermal::thermal_simulator *thermal_simulator_; /**< The thermal simulator attached to this simulation. */

			public:
				/**
				 * Constructor.
				 * @param a_thermal_simulator the thermal simulator attached to the simulation (optional argument)
				 */
				simulation(engine::thermal::thermal_simulator *a_thermal_simulator = nullptr) : thermal_simulator_(a_thermal_simulator) {}

				/**
				 * Creates a \ref test_injector for the given time-point
				 * @param index the time-point where the test vectors are inserted by the \ref test_injector
				 */
				test_injector operator[](quantity::time_type index) {
					return test_injector(test_vector_, index);
				}

				/**
				 * Attaches a thermal simulator to the simulation.
				 * @param a_thermal_simulator the thermal simulator to be attached to the system
				 */
				void set_thermal_simulator(engine::thermal::thermal_simulator &a_thermal_simulator) {thermal_simulator_ = &a_thermal_simulator;}

				/**
				 * Runs the simulation.
				 * The logi-thermal simulation is realized in this method.
				 * @param start the beginning of the simulation
				 * @param end the end of the simulation
				 * @param step the time_step in the simulation
				 * @param ambient_temperature the ambient temperature
				 */
				void run(quantity::time_type const &start, quantity::time_type const &end, quantity::time_type const &step, double ambient_temperature = 0.0) {
					component::set_ambient_temperature(ambient_temperature);

					component::start_simulation();

					for (quantity::time_type now = start; now != end; now += step) {
						component::set_time(now);
						component::start_simulation_cycle();

						auto tests = test_vector_.find(now);
						if (tests != test_vector_.end()) {
							for (auto &f: tests->second) {
								f();
							}
						}

						component::end_simulation_cycle();

						if (thermal_simulator_) thermal_simulator_->refresh_temperatures(now);
					}
				}
		};
	}
}

#endif //ENGINE_LOGIC_SIMULATION

