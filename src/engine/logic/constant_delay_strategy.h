/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_CONSTANTDELAYSTRATEGY
#define ENGINE_LOGIC_CONSTANTDELAYSTRATEGY

#include <quantity/time_type.h>
#include <engine/logic/delay_strategy.h>

namespace engine {
	namespace logic {
		/**
		 * Realizes a delay that is independent of the temperature.
		 */
		class constant_delay_strategy : public delay_strategy {
			private:
				quantity::time_type delay_; /**< The constant delay of the port. */

			public:
				/**
				 * Constructor.
				 * @param delay the constant delay of the component
				 */
				constant_delay_strategy(quantity::time_type const &delay) : delay_(delay) {}

				/**
				 * @copydoc engine::logic::delay_strategy::get_delay
				 */
				virtual quantity::time_type get_delay(double ambient_temperature, double temperature) override {return delay_;}
		};
	}
}

#endif // ENGINE_LOGIC_CONSTANTDELAYSTRATEGY

