/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_CONSTANTDISSIPATIONSTRATEGY
#define ENGINE_LOGIC_CONSTANTDISSIPATIONSTRATEGY

#include <vector>

#include <engine/logic/port.h>

namespace engine {
	namespace logic {
		/**
		 * Represents a constant dissipation strategy.
		 * Every time a component is considered to have been active in the
		 * latest simulation cycle by the activity_strategy, it will dissipate
		 * the value stored in \ref dissipation_.
		 */
		class constant_dissipation_strategy : public dissipation_strategy {
			private:
				double dissipation_; /**< The constant dissipation of the component. */
			public:
				/**
				 * Constructor.
				 * @param dissipation the dissipation of the component
				 */
				constant_dissipation_strategy(double dissipation = 0.0) : dissipation_(dissipation) {}

				/**
				 * @copydoc engine::logic::dissipation_strategy::get_dissipation
				 */
				virtual double get_dissipation(std::vector<port *> const &ports, double ambient_temperature, double temperature) const override {return dissipation_;}

				/**
				 * Destructor.
				 */
				virtual ~constant_dissipation_strategy() {} 
		};
	}
}

#endif //ENGINE_LOGIC_CONSTANTDISSIPATIONSTRATEGY


