/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_INPUTPORT
#define ENGINE_LOGIC_INPUTPORT

#include <engine/logic/port.h>
#include <quantity/time_type.h>

namespace engine {
	namespace logic {
		/**
		 * Base class of the input ports of a component.
		 * Unlike \ref port,  it is typed, so it can be read and written.
		 */
		template <typename data_type>
			class input_port : public port {
				public:
					using port_data_type = data_type; // The port's data type.

				public:
					/**
					 * Write a given value to the port.
					 * @param value the new value of the port.
					 */
					virtual void write(data_type const &value) = 0;

					/**
					 * Reads the value of the port.
					 * @return the value of the port
					 */
					virtual data_type const &read() const = 0;

					/**
					 * @copydoc engine::logic::port::get_direction
					 */
					virtual port::direction get_direction() const override {return port::direction::input;}

					/**
					 * @copydoc engine::logic::port::set_ambient_temperature
					 */
					void set_ambient_temperature(double const &ambient_temperature) override {};

					/**
					 * @copydoc engine::logic::port::set_temperature
					 */
					void set_temperature(double const &temperature) override {};
			};
	}
}

#endif //ENGINE_LOGIC_INPUTPORT
