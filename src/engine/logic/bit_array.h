/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_BITARRAY
#define ENGINE_LOGIC_BITARRAY

#include <array>
#include <string>
#include <algorithm>

#include <engine/logic/bit.h>

namespace engine {
	namespace logic {
		/**
		 * Represents a vector of bits.
		 * @tparam bits the number of bits in the vector
		 */
		template <size_t bits>
			class bit_array {
				private:
					std::array<bit, bits> value_; /**< The value of the bit_array. */

				public:
					bit_array(bit const &a_bit = bit()) {
						std::fill(value_.begin(), value_.end(), a_bit);
					}

					bit_array(std::string const &bit_string) {
						for (size_t i = 0; i < bits; ++i) {
							value_[bits - i - 1] = bit(bit_string[i]);
						}
					}

					bit_array(char const *bit_string) : bit_array<bits>(std::string(bit_string)) {}

					bit_array(unsigned long long an_integer) {
						unsigned_to_bit_array(an_integer);
					}

					bit_array(int an_integer) {
						unsigned_to_bit_array(static_cast<unsigned long long>(an_integer));
					}

					/**
					 * Converts the bit_array to an unsigned value.
					 * @throw std::domain_error if the bit_array contains undefined or high_impedance values.
					 */
					unsigned long long to_unsigned() const {
						unsigned long long an_integer = 0;

						for (size_t i = 0; i < bits; ++i) {
							bit a_bit = value_[bits - i - 1];
							if (a_bit.get_value() == bit::value::undefined || a_bit.get_value() == bit::value::high_impedance) throw std::domain_error("A bit_array contains either undefined or high_impedance bits so it cannot be converted to unsigned."); 

							an_integer <<= 1;
							if (a_bit == bit::value::high) an_integer |= 1;
						}

						return an_integer;
					}

					bit &operator [](size_t index) {return value_[index];}
					bit const &operator [](size_t index) const {return value_[index];}

					bit_array<bits> &operator +=(bit_array<bits> const &rhs) {
						try {
							unsigned long long value_lhs = to_unsigned();
							unsigned long long value_rhs = rhs.to_unsigned();
							unsigned_to_bit_array(value_lhs + value_rhs);
						}
						catch (std::domain_error const &de) {
							std::fill(value_.begin(), value_.end(), bit());
						}

						return *this;
					}

					bit_array<bits> &operator -=(bit_array<bits> const &rhs) {
						try {
							unsigned long long value_lhs = to_unsigned();
							unsigned long long value_rhs = rhs.to_unsigned();
							unsigned_to_bit_array(value_lhs - value_rhs);
						}
						catch (std::domain_error const &de) {
							std::fill(value_.begin(), value_.end(), bit());
						}

						return *this;
					}

					bit_array<bits> &operator *=(bit_array<bits> const &rhs) {
						try {
							unsigned long long value_lhs = to_unsigned();
							unsigned long long value_rhs = rhs.to_unsigned();
							unsigned_to_bit_array(value_lhs * value_rhs);
						}
						catch (std::domain_error const &de) {
							std::fill(value_.begin(), value_.end(), bit());
						}

						return *this;
					}

					bit_array<bits> &operator /=(bit_array<bits> const &rhs) {
						try {
							unsigned long long value_lhs = to_unsigned();
							unsigned long long value_rhs = rhs.to_unsigned();
							unsigned_to_bit_array(value_lhs / value_rhs);
						}
						catch (std::domain_error const &de) {
							std::fill(value_.begin(), value_.end(), bit());
						}

						return *this;
					}

					bit_array<bits> &operator &=(bit_array<bits> const &rhs) {
						for (size_t i = 0; i < bits; ++i) {
							value_[i] &= rhs.value_[i];
						}

						return *this;
					}

					bit_array<bits> &operator |=(bit_array<bits> const &rhs) {
						for (size_t i = 0; i < bits; ++i) {
							value_[i] |= rhs.value_[i];
						}

						return *this;
					}


					bit_array<bits> &operator ^=(bit_array<bits> const &rhs) {
						for (size_t i = 0; i < bits; ++i) {
							value_[i] ^= rhs.value_[i];
						}

						return *this;
					}

				private:
					void unsigned_to_bit_array(unsigned long long an_integer) {
						std::size_t an_integer_bits = sizeof(an_integer) * 8;
						std::size_t max_index = std::min(bits, an_integer_bits);

						for (size_t i = 0; i < max_index; ++i) {
							value_[i] = (an_integer % 2 == 0) ? bit::value::low : bit::value::high;
							an_integer >>= 1;
						}
					}

			};

		template <size_t bits>
			inline bit_array<bits> operator +(bit_array<bits> lhs, bit_array<bits> const &rhs) {return lhs += rhs;}

		template <size_t bits>
			inline bit_array<bits> operator -(bit_array<bits> lhs, bit_array<bits> const &rhs) {return lhs -= rhs;}

		template <size_t bits>
			inline bit_array<bits> operator *(bit_array<bits> lhs, bit_array<bits> const &rhs) {return lhs *= rhs;}

		template <size_t bits>
			inline bit_array<bits> operator /(bit_array<bits> lhs, bit_array<bits> const &rhs) {return lhs /= rhs;}

		template <size_t bits>
			inline bit_array<bits> operator &(bit_array<bits> lhs, bit_array<bits> const &rhs) {return lhs &= rhs;}

		template <size_t bits>
			inline bit_array<bits> operator |(bit_array<bits> lhs, bit_array<bits> const &rhs) {return lhs |= rhs;}

		template <size_t bits>
			inline bit_array<bits> operator ^(bit_array<bits> lhs, bit_array<bits> const &rhs) {return lhs ^= rhs;}

		template <size_t bits>
			inline bool operator ==(bit_array<bits> const &lhs, bit_array<bits> const &rhs) {
				for (size_t i = 0; i < bits; ++i) 
					if (lhs[i] != rhs[i]) return false;

				return true;
			}

		template <size_t bits>
			inline bool operator ==(bit_array<bits> const &lhs, std::string const &rhs) {return lhs == bit_array<bits>(rhs);}

		template <size_t bits>
			inline bool operator ==(std::string const &lhs, bit_array<bits> const &rhs) {return bit_array<bits>(lhs) == rhs;}

		template <size_t bits>
			inline bool operator !=(bit_array<bits> const &lhs, bit_array<bits> const &rhs) {return !(lhs == rhs);}

		template <size_t bits>
			std::ostream &operator <<(std::ostream &os, bit_array<bits> const &a_bit_array) {
				for (size_t i = 0; i < bits; ++i) os << a_bit_array[bits - i - 1];
				return os;
			}
	}
}

#endif //ENGINE_LOGIC_BITARRAY

