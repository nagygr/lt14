/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <string>
#include <fstream>
#include <sstream>
#include <utility>
#include <unordered_map>

#include <engine/logic/vcd_event_logger.h>

namespace engine {
	namespace logic {
		void vcd_event_logger::create_log() {
			std::unordered_map<std::string, std::pair<char, std::string>> variables;

			for (auto &l: loggers()) {
				variables[l->get_name()] = std::pair<char, std::string>('!' + variables.size(), l->get_vcd_type_size());
			}

			std::fstream vcd(file_name_, std::ios::out);

			quantity::time_type scale(1, time_scale_.get_unit());
			std::stringstream scale_str;
			scale_str << scale;

			vcd << "$timescale " << scale_str.str() << " $end" << std::endl;

			for (auto &v: variables) {
				vcd << "$var " << v.second.second << " " << v.second.first << " " << v.first << " $end" << std::endl;
			}

			for (auto &e: events()) {
				vcd << "#" << e.first << std::endl;
				for (auto &name_value: e.second) {
					vcd << name_value.second << " " << variables[name_value.first].first << std::endl;
				}
			}
		}
	}	
}
