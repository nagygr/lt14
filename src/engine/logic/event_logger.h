/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_EVENTLOGGER
#define ENGINE_LOGIC_EVENTLOGGER

#include <vector>
#include <memory>

#include <engine/logic/logger.h>
#include <engine/logic/event_log.h>
#include <engine/logic/typed_logger.h>
#include <engine/logic/sensitive_input_port.h>

namespace engine {
	namespace logic {
		/**
		 * Base class of loggers that can log an arbitrary number of ports.
		 * These classes contain a vector of \ref engine::logic::logger instances
		 * and the \ref engine::logic::event_log that the loggers work on.
		 */
		class event_logger {
			private:
				event_log events_; /**< The \ref engine::logic::event_log that the loggers work with. */
				std::vector<std::shared_ptr<logger>> loggers_; /**< The vector of loggers that log individual ports. */

			public:
				/**
				 * Provides an input port so that the logger can be connected to the output of components to be logged.
				 * Although the \ref engine::logic::event_logger class is not a component, it can be connected to
				 * components using this function. It instantiates a new \ref engine::logic::typed_logger and returns its
				 * inputs port so that is can be connected to the component's output that we want to log.
				 */
				template <typename data_type>
					sensitive_input_port<data_type> &log_as(std::string const &name) {
						auto new_logger = std::make_shared<typed_logger<data_type>>(name, events_);
						loggers_.push_back(new_logger);
						return new_logger->input;
					}

				/**
				 * Creates the actual log.
				 */
				virtual void create_log() = 0;

				/**
				 * Destructor.
				 */
				virtual ~event_logger() {}

			protected:
				/**
				 * Gets the \ref engine::logic::event_log.
				 * @return the \ref engine::logic::event_log
				 */
				event_log &events() {return events_;}

				/**
				 * Gets the container holding the loggers.
				 * @return the container holding the loggers
				 */
				std::vector<std::shared_ptr<logger>> &loggers() {return loggers_;}
		};
	}
}

#endif //ENGINE_LOGIC_EVENTLOGGER

