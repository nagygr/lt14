/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_DELTAINPUTPORT
#define ENGINE_LOGIC_DELTAINPUTPORT

#include <engine/logic/input_port.h>

namespace engine {
	namespace logic {
		/**
		 * Represents a port that introduces delta delay.
		 * These are the ports of a synchronous component that the component is not sensitive to.
		 * When a sensitive input changes and the component reads the value of this port, it will give the value
		 * of the previous simulation cycle. Any new value assigned to this port will only take effect when the
		 * simulation cycle ends (until then it is stored in \ref new_value_).
		 */
		template <typename data_type>
			class delta_input_port : public input_port<data_type> {
				private:
					data_type current_value_; /**< The current value of the port. */
					data_type new_value_; /**< The new value of the port that will take effect when the current simulation cycle ends. */

				public:
					/**
					 * Constructor.
					 */
					delta_input_port() : current_value_(), new_value_() {}

					/**
					 * @copydoc engine::logic::input_port::write
					 */
					virtual void write(data_type const &value) override {new_value_ = value; this->set_activity_flag();}

					/**
					 * @copydoc engine::logic::input_port::read
					 */
					virtual data_type const &read() const override {return current_value_;}

					/**
					 * @copydoc engine::logic::port::at_simulation_cycle_start
					 */
					virtual void at_simulation_cycle_start() override {}

					/**
					 * @copydoc engine::logic::port::at_simulation_cycle_end
					 */
					virtual void at_simulation_cycle_end() {current_value_ = new_value_;}

				protected:
					/**
					 * Gets the new value of the port.
					 * @return the new value of the port
					 */
					data_type const &get_new_value() const {return new_value_;}
			};
	}
}

#endif //ENGINE_LOGIC_DELTAINPUTPORT

