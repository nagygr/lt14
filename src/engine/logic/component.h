/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_COMPONENT
#define ENGINE_LOGIC_COMPONENT

#include <string>
#include <vector>
#include <memory>
#include <algorithm>

#include <engine/logic/port.h>
#include <quantity/time_type.h>
#include <engine/logic/activity_strategy.h>
#include <engine/logic/dissipation_strategy.h>

namespace engine {
	namespace logic {
		/**
		 * Base class of the logic components.
		 */
		class component {
			private:
				static std::vector<component *> components_; /**< Holds the address of every existing component. */
				static std::shared_ptr<activity_strategy> shared_activity_strategy_; /**< A shared activity strategy for the components that don't have a unique one. */
				static double ambient_temperature_; /**< The ambient temperature. */

			private:
				std::string name_; /**< The name of the component. */
				double temperature_; /**< The current temperature of the component as a difference to the \ref ambient_temperature_. */
				std::vector<port *> ports_; /**< Holds the address of the ports of the component. */
				std::shared_ptr<activity_strategy> unique_activity_strategy_; /**< The unique activity strategy of the component. */
				std::shared_ptr<dissipation_strategy> dissipation_strategy_; /**< The dissipation strategy of the component. */

			public:
				/**
				 * Constructor.
				 * @param name the name of the component
				 */
				component(std::string const &name) : name_(name), temperature_(0.0) {
					components_.push_back(this);	
				}

				/**
				 * The destructor of the component.
				 */
				virtual ~component() {
					components_.erase(std::find(components_.begin(), components_.end(), this));	
				}

				/**
				 * Gets the name of the component.
				 * @return the name of the component
				 */
				std::string const &get_name() const {return name_;}

				/**
				 * Returns whether the component was active in the current simulation cycle.
				 * @return whether the component was active in the current simulation cycle
				 */
				bool was_active() const {
					if (unique_activity_strategy_) return unique_activity_strategy_->was_active(ports_);
					else return shared_activity_strategy_->was_active(ports_);
				}

				/**
				 * Sets the unique activity strategy of the component.
				 * @param an_activity_strategy the unique activity strategy of the component.
				 */
				void set_unique_activity_strategy(std::shared_ptr<activity_strategy> an_activity_strategy) {
					unique_activity_strategy_ = an_activity_strategy;
				}

				/**
				 * Gets the dissipation of the component.
				 * @return the dissipation of the component
				 */
				double get_dissipation() const {
					if (dissipation_strategy_ && was_active()) return dissipation_strategy_->get_dissipation(ports_, ambient_temperature_, temperature_);
					else return 0.0;
				}

				/**
				 * Sets the dissipation strategy of the component.
				 * @param a_dissipation_strategy the dissipation strategy of the component.
				 */
				void set_dissipation_strategy(std::shared_ptr<dissipation_strategy> a_dissipation_strategy) {
					dissipation_strategy_ = a_dissipation_strategy;
				}

				/**
				 * Sets a \ref constant_dissipation_strategy for the component with the given dissipation value.
				 * @param dissipation the dissipation of the component when it is active
				 */
				void set_constant_dissipation(double dissipation);

				/**
				 * Sets the temperature of the component.
				 * @param temperature the temperature of the component.
				 */
				void set_temperature(double temperature) {temperature_ = temperature;}

				/**
				 * Gets the temperature of the component.
				 * @return temperature the temperature of the component.
				 */
				double get_temperature() const {return temperature_;}

				/**
				 * Sets the ambient temperature.
				 * @param temperature the ambient temperature.
				 */
				static void set_ambient_temperature(double temperature) {ambient_temperature_ = temperature;}

				/**
				 * Gets the ambient temperature.
				 * @return temperature the ambient temperature.
				 */
				static double get_ambient_temperature() {return ambient_temperature_;}

				/**
				 * Sets the current time of the component.
				 * This value is sent to every port.
				 * @param now the current time
				 */
				static void set_time(quantity::time_type const &now);

				/**
				 * Sets the address of the variables holding the ambient_temperature and the components' temperature
				 * in every port.
				 */
				static void start_simulation();

				/**
				 * Notifies every port of each gate that a new simulation cycle is about to begin.
				 */
				static void start_simulation_cycle();

				/**
				 * Notifies every port of each gate that the current simulation cycle has ended.
				 */
				static void end_simulation_cycle();

				/**
				 * Gets the container holding every component's address.
				 * @return the container holding every component's address
				 */
				static std::vector<component *> &get_components() {return components_;}

			protected:
				/**
				 * Gets the container holding the ports othe component.
				 * @return the container holding the ports othe component
				 */
				std::vector<port *> &ports() {return ports_;}
		};
	}
}

#endif //ENGINE_LOGIC_COMPONENT

