/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_LOGGER
#define ENGINE_LOGIC_LOGGER

#include <string>

#include <engine/logic/component.h>
#include <engine/logic/event_log.h>

namespace engine {
	namespace logic {
		/**
		 * Base class of loggers -- classes the are able to log a single port.
		 * It is a \ref engine::logic::component because it provides an input
		 * port so that it can be connected to normal components.
		 * It has a container (\ref logger::events_) that contains a vector of
		 * pairs of component names and values for any time-point in a map.
		 */
		class logger : public component {
			private:
				event_log &events_; //< Contains a vector of component names and values for every time-point of the simulation

			public:
				/**
				 * Constructor
				 * @param name the name of the logger
				 * @param events an \ref engine::logic::event_log that contains the actual events
				 */
				logger(std::string const &name, event_log &events) : component(name), events_(events) {}

				/**
				 * Gets the \ref events_.
				 * @return the \ref events_
				 */
				event_log &events() {return events_;}

				/**
				 * Returns the VCD type and size of the data type that is logged.
				 * This is only relevant in the \ref engine::logic::vcd_event_logger type
				 * but appears at the base class.
				 * @return the VCD type and size of the data type that is logged
				 */
				virtual std::string get_vcd_type_size() const = 0;

				/**
				 * Destructor.
				 */
				virtual ~logger() {}
		};
	}
}

#endif //ENGINE_LOGIC_LOGGER

