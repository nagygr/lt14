/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_PORT
#define ENGINE_LOGIC_PORT

#include <vector>

#include <quantity/time_type.h>

namespace engine {
	namespace logic {
		/**
		 * The base class of every port.
		 */
		class port {
			public:
				/**
				 * the direction of the port.
				 */
				enum class direction {
					input /**< input port */, 
					output /**< output port */
				};

			private:
				quantity::time_type time_; /**< The current time of the port. */
				bool was_active_; /**< Tells whether the port was active in the current simulation cycle. */

			public:
				/**
				 * Constructor.
				 */
				port() : was_active_(false) {}

				/**
				 * Sets the current time of the port.
				 * @param now the current time
				 */
				virtual void set_time(quantity::time_type const &now) {time_ = now; was_active_ = false;}

				/**
				 * A callback function that is called before a simulation cycle is started.
				 * It allows ports to get ready for the new simulation cycle.
				 */
				virtual void at_simulation_cycle_start() = 0;

				/**
				 * A callback function that is called when a simulation cycle ends.
				 * It allows ports to do clean up after the simulation cycle.
				 */
				virtual void at_simulation_cycle_end() = 0;

				/**
				 * Gets the direction of the port.
				 * @return the direction of the port
				 */
				virtual direction get_direction() const = 0;

				/**
				 * Sets the address of the variable holding the ambient temperature.
				 * @param ambient_temperature reference of the variable holding the ambient temperature
				 */
				virtual void set_ambient_temperature(double const &ambient_temperature) = 0;

				/**
				 * Sets the address of the variable holding the component's temperature.
				 * @param ambient_temperature reference of the variable holding the component's temperature
				 */
				virtual void set_temperature(double const &temperature) = 0;

				/**
				 * Destructor.
				 */
				virtual ~port() {}

				/**
				 * Tells whether the port was active in the current simulation cycle.
				 * @return whether the port was active in the current simulation cycle
				 */
				bool was_active() const {return was_active_;}

				/**
				 * Gets the current time of the port.
				 * @return the current time of the port
				 */
				quantity::time_type const &get_time() const {return time_;}

			protected:
				/**
				 * Sets the port as active. 
				 */
				void set_activity_flag() {was_active_ = true;}	
		};

		/**
		 * Adds the port to a vector of ports.
		 * @param ports the vector of ports
		 * @param a_port the port to be added to the vector
		 */
		std::vector<port *> &operator <<(std::vector<port *> &ports, port &a_port);
	}
}

#endif //ENGINE_LOGIC_PORT

