/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_SENSITIVEINPUTPORT
#define ENGINE_LOGIC_SENSITIVEINPUTPORT

#include <vector>
#include <functional>

#include <engine/logic/input_port.h>

namespace engine {
	namespace logic {
		/**
		 * Represents an input port of a synchronous component to which the component is sensitive to.
		 * Whenever a change occurs at this point, to component will react to it. This is ensured by a callback
		 * function stored in the port that it can call when its input changes.
		 * Actually several callbacks can be stored in the port because a component might react to a change in several ways
		 * (in VHDL terms, a component might have several processes that are sensitive to this port).
		 */
		template <typename data_type>
			class sensitive_input_port : public input_port<data_type> {
				private:
					data_type value_; /**< The value of the port. */
					std::vector<std::function<void()>> operations_; /**< The callback functinos that are called when a change occurs at this port. */

				public:
					/**
					 * Constructor.
					 */
					sensitive_input_port() : value_() {}

					/**
					 * Constructor.
					 * @param operation a callback function that should be called when the port changes
					 */
					sensitive_input_port(std::function<void()> operation) : sensitive_input_port() {
						operations_.push_back(operation);
					}

					/**
					 * @copydoc engine::logic::input_port::write 
					 */
					virtual void write(data_type const &value) override;

					/**
					 * @copydoc engine::logic::input_port::read
					 */
					virtual data_type const &read() const override {return value_;}

					/**
					 * @copydoc engine::logic::port::at_simulation_cycle_start
					 */
					virtual void at_simulation_cycle_start() override {}

					/**
					 * @copydoc engine::logic::port::at_simulation_cycle_end
					 */
					virtual void at_simulation_cycle_end() override {}

					/**
					 * Adds a new callback function.
					 * @param operation the new callback function
					 */
					void add_operation(std::function<void()> operation) {operations_.push_back(operation);}

					/**
					 * Clears the container of the callback functions.
					 */
					void clear_operations() {operations_.clear();}

					/**
					 * Adds a new callback function.
					 * @param operation the new callback function
					 */
					sensitive_input_port &operator <<(std::function<void()> operation) {
						operations_.push_back(operation);
						return *this;
					}
			};

		template <typename data_type>
			void sensitive_input_port<data_type>::write(data_type const &value) {
				if (value_ != value) {
					value_ = value;
					this->set_activity_flag();
					for (auto &f: operations_) if (f) f();
				}
			}
	}
}

#endif //ENGINE_LOGIC_SENSITIVEINPUTPORT
