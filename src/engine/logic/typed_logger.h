/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_TYPEDLOGGER
#define ENGINE_LOGIC_TYPEDLOGGER

#include <string>
#include <sstream>

#include <engine/logic/bit.h>
#include <engine/logic/bit_array.h>
#include <engine/logic/logger.h>
#include <engine/logic/event_log.h>
#include <engine/logic/sensitive_input_port.h>

namespace engine {
	namespace logic {
		/**
		 * Returns the VCD type and size of a class in a formatted string that conforms the VCD
		 * format. This class is specialized with the types to get the exact operation needed.
		 * The default return type and size is "real" and "32".
		 * @tparam data_type the type of the data that is logged
		 */
		template <typename data_type>
			class vcd_type_size_ {
				public:
					static std::string get_type_size() {
						return "real 32";
					}
			};

		/**
		 * Specialization of \ref engine::logic::vcd_type_size_ with the type bool.
		 */
		template <>
			class vcd_type_size_<bool> {
				public:
					static std::string get_type_size() {
						return "wire 1";
					}
			};

		/**
		 * Specialization of \ref engine::logic::vcd_type_size_ with the type bit.
		 */
		template <>
			class vcd_type_size_<bit> {
				public:
					static std::string get_type_size() {
						return "wire 1";
					}
			};

		/**
		 * Specialization of \ref engine::logic::vcd_type_size_ with the type bit_array<N>
		 * where N is a template parameter representing the size of the bit_array.
		 */
		template <size_t N>
			class vcd_type_size_<bit_array<N>> {
				public:
					static std::string get_type_size() {
						std::stringstream s;
						s << "wire " << N;
						return s.str();
					}
			};

		/**
		 * Helper function for the \ref engine::logic::vcd_type_size_ classes to
		 * ease to avoid having to explicitly state their template parameters.
		 */
		template <typename data_type>
			std::string vcd_type_size() {
				return vcd_type_size_<data_type>::get_type_size();
			}

		/**
		 * A typed logger class that acts as a component. It can log one single port.
		 * Its input port can be connected to any output
		 * and when the value at the input changes, it adds
		 * an entry to the event_log.
		 * @tparam data_type the type of the data that is logged
		 */
		template <typename data_type>
			class typed_logger : public logger {
				public:
					sensitive_input_port<data_type> input; /**< The input port of the logger component. */

				private:
					std::string type_size_; /**< The name and size of the data_type as a string conforming to the VCD format. */
					std::string type_letter_; /**< The letter preceeds the constants in the VCD format. */

				public:
					/**
					 * Constructor.
					 * @param name the name of the logger component
					 * @param events the \ref engine::logic::event_log where the events are written
					 */
					typed_logger(std::string const &name, event_log &events) : 
						logger(name, events), 
						input(std::bind(&typed_logger<data_type>::operation, this)), 
						type_size_(vcd_type_size<data_type>()), 
						type_letter_((type_size_.find("real") == 0) ? "r" : "b") {

						ports() << input;
						
					}

					/**
					 * The handler function of the value changes at the input port.
					 * This function stores the events in the \ref engine::logic::event_log.
					 */
					void operation() {
						std::stringstream value;
						value << type_letter_ << input.read();
						events()[input.get_time()].push_back(std::make_pair(get_name(), value.str()));
					}

					/**
					 * Gets the \ref type_size_.
					 * @return the \ref type_size_
					 */
					virtual std::string get_vcd_type_size() const override {
						return type_size_;
					}
			};
	}
}

#endif //ENGINE_LOGIC_TYPEDLOGGER

