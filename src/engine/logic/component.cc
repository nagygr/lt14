/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <engine/logic/component.h>
#include <engine/logic/output_port.h>
#include <engine/logic/output_activity_strategy.h>
#include <engine/logic/constant_dissipation_strategy.h>

namespace engine {
	namespace logic {
		std::vector<component *> component::components_;
		std::shared_ptr<activity_strategy> component::shared_activity_strategy_ = std::make_shared<output_activity_strategy>();
		double component::ambient_temperature_ = 0.0;

		void component::set_time(quantity::time_type const &now) {
			for (auto &c: components_) {
				for (auto &p: c->ports()) p->set_time(now);
			}
		}
		
		void component::start_simulation() {
			for (auto &c: components_) {
				for (auto &p: c->ports()) {
					p->set_ambient_temperature(ambient_temperature_);
					p->set_temperature(c->temperature_);
				}
			}
		}

		void component::start_simulation_cycle() {
			for (auto &c: components_) {
				for (auto &p: c->ports()) p->at_simulation_cycle_start();
			}
		}

		void component::end_simulation_cycle() {
			for (auto &c: components_) {
				for (auto &p: c->ports()) p->at_simulation_cycle_end();
			}
		}
		
		void component::set_constant_dissipation(double dissipation) {
			dissipation_strategy_ = std::make_shared<constant_dissipation_strategy>(dissipation);
		}
	}
}
