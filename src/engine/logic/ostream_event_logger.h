/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_OSTREAMEVENTLOGGER
#define ENGINE_LOGIC_OSTREAMEVENTLOGGER

#include <iostream>

#include <engine/logic/event_logger.h>

namespace engine {
	namespace logic {
		/**
		 * Simple \ref event_logger class that writes the log to an ostream.
		 */
		class ostream_event_logger : public event_logger {
			private:
				std::ostream &os_; /**< The target ostream. */

			public:
				/**
				 * Constructor.
				 * @param os the target ostream
				 */
				ostream_event_logger(std::ostream &os) : os_(os) {}

				/**
				 * @copydoc engine::logic::event_logger::create_log
				 */
				virtual void create_log() override {
					for (auto &e: events()) {
						os_ << "<<< " << e.first << " >>>" << std::endl;
						for (auto name_value: e.second) {
							os_ << "\t" << name_value.first << ": " << name_value.second << std::endl;
						}
					}
				}
		};
	}
}

#endif //ENGINE_LOGIC_OSTREAMEVENTLOGGER
