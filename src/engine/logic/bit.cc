/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <sstream>
#include <iostream>
#include <stdexcept>

#include <engine/logic/bit.h>

namespace engine {
	namespace logic {
		std::array<std::pair<bit::value, char>, 4> bit::value_map_ = { 
			std::pair<bit::value, char>(bit::value::undefined, 'x'), 
			std::pair<bit::value, char>(bit::value::low, '0'),
			std::pair<bit::value, char>(bit::value::high, '1'),
			std::pair<bit::value, char>(bit::value::high_impedance, 'z')
		};

		bit::bit (char c) {
			auto a_value = std::find_if(value_map_.begin(), value_map_.end(), [=](std::pair<value, char> const &v) {return v.second == c;});

			if (a_value == value_map_.end()) {
				std::stringstream message;
				message << "Invalid argument to bit constructor (" << c << ")";
				throw std::invalid_argument(message.str());
			}

			value_ = a_value->first;
		}

		bit &bit::operator &=(bit const &rhs) {
			if (value_ == bit::value::undefined || rhs.get_value() == bit::value::undefined) value_ = value::undefined;
			else if (value_ == bit::value::high_impedance || rhs.get_value() == bit::value::high_impedance) value_ = value::undefined;
			else if (value_ == value::high && rhs.value_ == value::high) value_ = value::high;
			else value_ = value::low;
			return *this;
		}

		bit &bit::operator |=(bit const &rhs) {
			if (value_ == value::high || rhs.value_ == value::high) value_ = value::high;
			else if (value_ == bit::value::undefined || rhs.get_value() == bit::value::undefined) value_ = value::undefined;
			else if (value_ == bit::value::high_impedance || rhs.get_value() == bit::value::high_impedance) value_ = value::undefined;
			else value_ = value::low;
			return *this;
		}

		bit &bit::operator ^=(bit const &rhs) {
			if ((value_ == value::high && rhs.value_ == value::low) || (value_ == value::low && rhs.value_ == value::high)) value_ = value::high;
			else if (value_ == bit::value::undefined || rhs.get_value() == bit::value::undefined) value_ = value::undefined;
			else if (value_ == bit::value::high_impedance || rhs.get_value() == bit::value::high_impedance) value_ = value::undefined;
			else value_ = value::low;
			return *this;
		}
		
		bool operator ==(bit const &lhs, bit const &rhs) {
			if (lhs.get_value() == bit::value::undefined || rhs.get_value() == bit::value::undefined) return false;
			if (lhs.get_value() == bit::value::high_impedance || rhs.get_value() == bit::value::high_impedance) return false;
			if (lhs.get_value() == rhs.get_value()) return true;
			return false;
		}

		std::ostream &operator <<(std::ostream &os, bit const &a_bit) {
			return os << std::find_if(bit::value_map_.begin(), bit::value_map_.end(), [=](std::pair<bit::value, char> const &v) {return v.first == a_bit.get_value();})->second;
		}
	}
}
