/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_VCDEVENTLOGGER
#define ENGINE_LOGIC_VCDEVENTLOGGER

#include <string>

#include <quantity/time_type.h>
#include <engine/logic/event_logger.h>

namespace engine {
	namespace logic {
		/**
		 * An event logger that creates a VCD output. 
		 */
		class vcd_event_logger : public event_logger {
			private:
				std::string file_name_; /**< The name of the VCD file to be created. */
				quantity::time_type time_scale_; /**< The time scale of the simulation (goes to the VCD file). */

			public:
				/**
				 * Constructor.
				 * @param file_name the name of the VCD file
				 * @param time_scale the time scale of th simulation
				 */
				vcd_event_logger(std::string const &file_name, quantity::time_type const &time_scale = quantity::time_type(1, quantity::time_type::unit::us)) : 
					file_name_(file_name), 
					time_scale_(time_scale) {}

				/**
				 * @copydoc engine::logic::event_logger::create_log
				 */
				virtual void create_log() override;
		};
	}
}

#endif //ENGINE_LOGIC_VCDEVENTLOGGER

