/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_OUTPUTPORT
#define ENGINE_LOGIC_OUTPUTPORT

#include <vector>
#include <memory>
#include <utility>

#include <engine/logic/port.h>
#include <quantity/time_type.h>
#include <engine/logic/input_port.h>
#include <engine/logic/delay_strategy.h>
#include <engine/logic/constant_delay_strategy.h>

namespace engine {
	namespace logic {
		/**
		 * The output port of components.
		 */
		template <typename data_type>
			class output_port : public port {
				public:
					using port_data_type = data_type; /**< The data type of the component. */

				private:
					data_type value_; /**< The current value of the output port. */
					std::vector<input_port<data_type> *> connections_; /**< The input ports that the port is connected to. */
					std::shared_ptr<delay_strategy> delay_; /**< The delay strategy of the component. */
					bool next_value_set_; /**< Tells whether the next value of the port has been scheduled to a time-point in the future. */
					std::pair<quantity::time_type, data_type> next_value_; /**< the next value of the port scheduled to a time-point in the future. */
					double const *ambient_temperature_; /**< Address of the variable holding the ambient temperature of the system. */
					double const *temperature_; /**< Address of the variable holding the component's temperature. */

				public:
					/**
					 * Default constructor.
					 */
					output_port() : value_(), delay_(), next_value_set_(false), ambient_temperature_(nullptr), temperature_(nullptr) {}
					/**
					 * Constructor.
					 * @param delay the delay that is introduced by the port
					 */
					output_port(quantity::time_type const &delay) : output_port() {
						delay_ = std::make_shared<constant_delay_strategy>(delay);
					}

					/**
					 * Writes the given value to the port.
					 * @param value the new value of the port
					 */
					void write(data_type const &value);

					/**
					 * Reads the current value of the port.
					 * @return the current value of the port
					 */
					data_type const &read() const {return value_;}

					/**
					 * Connects an input port to this port.
					 * @param port the input port that is connected to this port
					 */
					void connect(input_port<data_type> &port) {connections_.push_back(&port);}

					/**
					 * Gets the delay of the port.
					 * @return the delay of the port
					 */
					quantity::time_type get_delay();

					/**
					 * @copydoc engine::logic::port::set_ambient_temperature
					 */
					void set_ambient_temperature(double const &ambient_temperature) override {ambient_temperature_ = &ambient_temperature;}

					/**
					 * @copydoc engine::logic::port::set_temperature
					 */
					void set_temperature(double const &temperature) override {temperature_ = &temperature;}

					/**
					 * Sets the delay strategy.
					 * @param a_delay_strategy a delay strategy
					 */
					void set_delay_strategy(std::shared_ptr<delay_strategy> a_delay_strategy) {delay_ = a_delay_strategy;}

					/**
					 * @copydoc engine::logic::port::at_simulation_cycle_start
					 */
					virtual void at_simulation_cycle_start() override;

					/**
					 * @copydoc engine::logic::port::at_simulation_cycle_end
					 */
					virtual void at_simulation_cycle_end() override {}

					/**
					 * @copydoc engine::logic::port::get_direction
					 */
					virtual port::direction get_direction() const override {return port::direction::output;}
			};

		template <typename data_type>
			void output_port<data_type>::write(data_type const &value) {
				quantity::time_type delay = get_delay();

				if (delay == quantity::time_type(0)) {
					value_ = value;
					set_activity_flag();
					for (auto &c: connections_) {
						c->write(value_);
					}
				}
				else {
					next_value_set_ = true;
					next_value_ = std::make_pair(get_time() + delay, value);
				}
			}

		template <typename data_type>
			void output_port<data_type>::at_simulation_cycle_start() {
				if (next_value_set_ && get_time() == next_value_.first) {
					value_ = next_value_.second;
					set_activity_flag();
					next_value_set_ = false;
					for (auto &c: connections_) {
						c->write(value_);
					}
				}
			}

		template <typename data_type>
			quantity::time_type output_port<data_type>::get_delay() {
				if (delay_) return delay_->get_delay(*ambient_temperature_, *temperature_);
				else return quantity::time_type(0);
			}
	}
}

#endif //ENGINE_LOGIC_OUTPUTPORT

