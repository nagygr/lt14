/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENGINE_LOGIC_BIT
#define ENGINE_LOGIC_BIT

#include <utility>
#include <iostream>
#include <algorithm>
#include <unordered_map>

namespace engine {
	namespace logic {
		/**
		 * Represents a bit in the logic simulation.
		 */
		class bit {
			public:	
				/**
				 * The possibles values a bit can take.
				 */
				enum class value {
					undefined /**< the bit has not been given a valid value or the output of an operation was undefined */,
					low /**< logic false */,
					high /**< logic true */,
					high_impedance /**< high impedance */
				};
				static std::array<std::pair<value, char>, 4> value_map_; /**< Maps values to characters. */

			private:
				value value_; /**< The value of the bit. */

			public:
				/**
				 * Constructor.
				 * Initializes the bit using a \ref bit::value.
				 * @param value the initial value of the bit
				 */
				bit(value a_value = value::undefined) : value_(a_value) {}

				/**
				 * Constructor.
				 * Initializes the bit using a character
				 * @param c the initial value of the bit
				 */
				bit (char c);

				/**
				 * Constructor.
				 * Initializes the bit using a bool
				 * @param b the initial value of the bit
				 */
				bit(bool b) : value_(b ? value::high : value::low) {}

				/**
				 * Gets the value of the bit.
				 * @return the value of the bit
				 */
				value get_value() const {return value_;}	

				bit &operator &=(bit const &rhs);
				bit &operator |=(bit const &rhs);
				bit &operator ^=(bit const &rhs);

		};

		inline bit operator &(bit lhs, bit const &rhs) {return lhs &= rhs;}
		inline bit operator |(bit lhs, bit const &rhs) {return lhs |= rhs;}
		inline bit operator ^(bit lhs, bit const &rhs) {return lhs ^= rhs;}

		bool operator ==(bit const &lhs, bit const &rhs);
		inline bool operator ==(bit const &lhs, bit::value const &rhs) {return lhs == bit(rhs);}
		inline bool operator ==(bit::value const &lhs, bit const &rhs) {return rhs == lhs;}
		inline bool operator !=(bit const &lhs, bit const &rhs) {return !(lhs == rhs);}
		inline bool operator !=(bit const &lhs, bit::value const &rhs) {return !(lhs == rhs);}
		inline bool operator !=(bit::value const &lhs, bit const &rhs) {return !(lhs == rhs);}

		std::ostream &operator <<(std::ostream &os, bit const &a_bit);
	}
}

inline engine::logic::bit operator "" _b(char const *the_string, size_t) {
	return engine::logic::bit(*the_string);
}

#endif //ENGINE_LOGIC_BIT
