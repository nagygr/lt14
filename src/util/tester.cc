/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <iostream>

#include <util/tester.h>

namespace util {
	std::list< std::pair<std::string, tester::test_function> > tester::the_tests;

	size_t tester::run_tests() {
		size_t test_no = 0, failed = 0;

		/**
		 * Represents a failure record.
		 */
		struct test_failure {
			std::string the_file_name;
			size_t the_test_no;
			std::string the_message;

			test_failure(std::string const &file_name, size_t test_no, std::string const &message) :
				the_file_name(file_name),
				the_test_no(test_no),
				the_message(message) {}
		};

		/* 
		 * List containing the list of failed tests.
		 */
		std::list< test_failure > failed_tests;
		
		std::string current_file = ""; /* The name of the current test file. */
		size_t local_test_no = 0; /* The number of the current test within the current file. */

		for (auto &test : the_tests) {
			if (test.first == current_file) ++local_test_no;
			else {
				current_file = test.first;
				local_test_no = 0;
			}

			std::cerr << std::endl << "************************************************************************";
			std::cerr << "\nRunning test no. " << test_no << "\t(\"" << current_file << "\" [" << local_test_no << "])";
			std::cerr << "\n************************************************************************";
			std::cerr << std::endl << std::endl;

			test_result result = test.second();
			
			std::cerr << std::endl << "*****************************\nTest no. " << test_no;
			std::cerr << ": " << (result ? "SUCCEDED" : "FAILED") << std::endl;
			if (!result) {
				std::cerr << "\tError message: " << result.the_message << std::endl;
			}
			std::cerr << "*****************************" << std::endl;
			std::cerr << std::endl << "#######################################################################################" << std::endl;

			if (!result) {
				++failed;
				failed_tests.push_back(test_failure(current_file, test_no, result.the_message));
			}

			++test_no;
		}

		if (failed == 0)
			std::cerr << std::endl << "SUMMARY: Every test (" << test_no << ") SUCCEDED." << std::endl;
		else {
			std::cerr << std::endl << "SUMMARY: " << failed << " FAILED out of " << test_no << " test" << (test_no > 1 ? "s." : ".") << std::endl;

			std::cerr << std::endl << "\tThe failed test(s):" << std::endl;
			for (auto &failed : failed_tests) {
				std::cerr << "\t\t" << " Test number " << failed.the_test_no << " in \"" << failed.the_file_name << "\"" << std::endl << "\t\t\tError message: " << failed.the_message << std::endl;
			}
			std::cerr << std::endl;
		}

		return failed;
	}
}
