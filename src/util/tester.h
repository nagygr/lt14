/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014, Gergely Nagy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef UTIL_TESTER
#define UTIL_TESTER

#include <list>
#include <string>
#include <cstddef>
#include <utility>
#include <functional>

namespace util {
	/**
	 * Simple tester class.
	 */
	class tester {
		public:
			/**
			 * The result of the tests that makes it possible to attach az error message to the logic value.
			 */
			struct test_result {
				bool the_value; /**< The result of the test. */
				std::string the_message; /**< The error message. */

				/**
				 * Constructor.
				 * @param a_value the result of the test
				 * @param a_message the error message
				 */
				test_result(bool a_value, std::string const &a_message = "") :
					the_value(a_value),
					the_message(a_message) {}

				/**
				 * Operator converting the result to a bool value.
				 * @return true if the test passed, false if it failed.
				 */
				operator bool() const {return the_value;}
			};
			/**
			 * Test function type.
			 */
			using test_function = std::function<test_result()>; 

		private:
			/**
			 * List containing the test functions.
			 * It contains pairs where the first element is the name of
			 * the file where the test resides (as a std::string),
			 * the second element is the \ref util::tester::test_function itself.
			 */
			static std::list< std::pair<std::string, test_function> > the_tests; 

		public:
			/**
			 * Adds a function to the list of tests.
			 * @param filename the file where the test resides (the __FILE__ macro should
			 * be used to provide it).
			 * @param a_function the function to be added
			 */
			static void add_test(std::string const &filename, test_function const &a_function) {
				the_tests.push_back(std::make_pair(filename, a_function));
			}

			/**
			 * Runs the tests.
			 * @return the number of failed tests
			 */
			static size_t run_tests();
	};
}

#endif // UTIL_TESTER
