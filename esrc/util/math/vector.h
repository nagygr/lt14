#ifndef __UTIL_MATH_VECTOR__
#define __UTIL_MATH_VECTOR__

#include <iostream>
#include <iterator>

namespace util {
	namespace math {
		template <typename T>
			class vector {
				public:
					class iterator : public std::iterator<std::forward_iterator_tag, T> {
						private:
							vector *owner;
							unsigned index;
							unsigned end_index;

						public:
							iterator() {}

							iterator(vector *owner, unsigned index, const unsigned end_index) : owner(owner), index(index), end_index(end_index) {}

							bool operator ==(const iterator &other) const {
								return index == other.index;
							}

							bool operator !=(const iterator &other) const {
								return index != other.index;
							}

							iterator &operator ++() {
								if (index != end_index) ++index;
								return *this;
							}

							iterator operator ++(int) {
								iterator tmp(*this);
								if (index != end_index) ++index;
								return tmp;
							}

							T &operator *() {
								return owner->data[index];
							}

							T *operator ->() {
								return &(owner->data[index]);
							}
					};

					class const_iterator : public std::iterator<std::forward_iterator_tag, T> {
						private:
							const vector *owner;
							unsigned index;
							unsigned end_index;

						public:
							const_iterator() {}

							const_iterator(const vector *owner, unsigned index, const unsigned end_index) : owner(owner), index(index), end_index(end_index) {}

							bool operator ==(const const_iterator &other) const {
								return index == other.index;
							}

							bool operator !=(const const_iterator &other) const {
								return index != other.index;
							}

							const_iterator &operator ++() {
								if (index != end_index) ++index;
								return *this;
							}

							const_iterator operator ++(int) {
								iterator tmp(*this);
								if (index != end_index) ++index;
								return tmp;
							}

							const T &operator *() {
								return owner->data[index];
							}

							const T *operator ->() {
								return &(owner->data[index]);
							}
					};

				protected:
					T *data;
					unsigned size;

				public:
					explicit vector(unsigned size = 0) : size(size) {
						data = new T[size];
					}

					vector(unsigned size, T value);

					vector(const vector &v);

					vector &operator =(const vector &rhs);
					//vector &operator =(const T &rhs);

					void set_all_elements_to(const T &rhs);

					iterator begin() {
						return iterator(this, 0, size);
					}

					iterator end() {
						return iterator(this, size, size);
					}

					const_iterator begin() const {
						return const_iterator(this, 0, size);
					}

					const_iterator end() const {
						return const_iterator(this, size, size);
					}

					virtual T &operator [](unsigned index) {return data[index];}
					virtual const T &operator [](unsigned index) const {return data[index];}
					virtual unsigned get_size() const {return size;}
					virtual T *get_data() {return data;}

					virtual vector operator +(const vector &rhs) const;
					virtual vector &operator +=(const vector &rhs);

					virtual bool operator ==(const vector &rhs) const;
					virtual bool operator !=(const vector &rhs) const;

					virtual ~vector() {
						delete []data;
					}

				private:
					vector(T *data, unsigned size) : data(data), size(size) {}
			};

		template <typename T>
			vector<T>::vector(unsigned size, T value) : size(size) {
				data = new T[size];

#pragma omp parallel for schedule(dynamic)
				for (unsigned i = 0; i < size; ++i) {
					data[i] = value;
				}
			}

		template <typename T>
			vector<T>::vector(const vector &v) {
				data = 0;
				size = 0;
				*this = v;
			}

		template <typename T>
			vector<T> &vector<T>::operator=(const vector &rhs) {
				if (this != &rhs) {
					if (size != rhs.size) {
						delete []data;
						data = new T[size = rhs.size];
					}
#pragma omp parallel for schedule(dynamic)
					for (unsigned i = 0; i < size; ++i) {
						data[i] = rhs[i];
					}
				}
				return *this;
			}

		/*
		template <typename T>
			vector<T> &vector<T>::operator =(const T &rhs) {
#pragma omp parallel for schedule(dynamic)
				for (unsigned i = 0; i < size; ++i) {
					data[i] += rhs;
				}
				return *this;
			}
			*/
		template <typename T>
			void vector<T>::set_all_elements_to(const T &rhs) {
				for (unsigned i = 0; i < size; ++i) {
					data[i] = rhs;
				}
			}

		template <typename T>
			vector<T> vector<T>::operator +(const vector<T> &rhs) const {
				T *new_data = new T[size];
#pragma omp parallel for schedule(dynamic)
				for (unsigned i = 0; i < size; ++i) new_data[i] = data[i] + rhs.data[i];
				return vector<T>(new_data, size);
			}

		template <typename T>
			vector<T> &vector<T>::operator +=(const vector<T> &rhs) {
#pragma omp parallel for schedule(dynamic)
				for (unsigned i = 0; i < size; ++i) {
					data[i] += rhs.data[i];
				}
				return *this;
			}

		template <typename T>
			bool vector<T>::operator ==(const vector<T> &rhs) const {
				for (unsigned i = 0; i < size; ++i) {
					if (data[i] != rhs.data[i]) return false;
				}
				return true;
			}

		template <typename T>
			bool vector<T>::operator !=(const vector<T> &rhs) const {
				for (unsigned i = 0; i < size; ++i) {
					if (data[i] != rhs.data[i]) return true;
				}
				return false;
			}
	}
}

template <typename T>
std::ostream &operator <<(std::ostream &os, const util::math::vector<T> &v) {
	for (unsigned i = 0; i < v.get_size(); ++i) {
		os << v[i] << " ";
	}

	return os;
}

#endif //__UTIL_MATH_VECTOR__

