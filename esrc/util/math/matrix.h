#ifndef __UTIL_MATH_MATRIX__
#define __UTIL_MATH_MATRIX__

#include <iostream>
#include <util/math/vector.h>

namespace util {
	namespace math {
		template <typename T>
			class matrix {
				private:
					T *data;
					unsigned rows;
					unsigned cols;

				public:
					matrix(unsigned rows, unsigned cols) : rows(rows), cols(cols) {
						T value(0);
						data = new T[rows * cols];
#pragma omp parallel for schedule(static)
						for (unsigned i = 0; i < rows * cols; ++i) {
							data[i] = value;
						}
					}

					matrix(const matrix &m) {
						data = new T[(rows = m.rows) * (cols = m.cols)];
#pragma omp parallel for schedule(static)
						for (unsigned i = 0; i < rows * cols; ++i) data[i] = m.data[i];
					}

					~matrix() {delete[] data;}

					matrix &operator=(const matrix &m) {
						if (this != &m) {
							delete []data;
							data = new T[(rows = m.rows) * (cols = m.cols)];
#pragma omp parallel for schedule(static)
							for (unsigned i = 0; i < rows * cols; ++i) data[i] = m.data[i];
						}

						return *this;
					}

					unsigned get_rows() const {return rows;}
					unsigned get_cols() const {return cols;}
					T *get_data() {return data;}

					T &operator()(unsigned i, unsigned j) {
						return *(data + (cols * i) + j);
					}

					const T &operator()(unsigned i, unsigned j) const {
						return *(data + (cols * i) + j);
					}

					matrix operator *(const matrix &m) const {
						//if (cols != m.rows) throw std::out_of_range("Matrix dimensions don't match.");

						matrix result(rows, m.cols);
						T value(0);
						unsigned i, j, k;

#pragma omp parallel for private(i, j, k, value) schedule(static)
						for (i = 0; i < rows; ++i) {
							for (j = 0; j < m.cols; ++j) {
								for (k = 0; k < cols; ++k) {
									value += (*this)(i, k) * m(k, j);
								}

								result(i, j) = value;
								value = T(0);
							}
						}

						return result;
					}

					vector<T> operator *(const vector<T> &v) const {
						//if (cols != v.size()) throw std::out_of_range("Matrix dimensions don't match");

						vector<T> result(rows);

						for (unsigned i = 0; i < rows; ++i) {
							T value(0);

#pragma omp parallel for reduction(+:value) schedule(static)
							for (unsigned j = 0; j < cols; ++j) {
								value += (*this)(i, j) * v[j];
							}
							result[i] = value;
						}

						return result;
					}

					matrix get_lu() const;

					static vector<T> solve(const matrix &lu, const vector<T> &b);
			};

		template <typename T>
			matrix<T> matrix<T>::get_lu() const {
				matrix<T> lu(*this);

				//if (rows != cols) throw std::out_of_range("Not a square matrix");
				unsigned size = rows;

				for (unsigned k = 1; k < size; ++k) {
					T value = lu(k - 1, k - 1);
#pragma omp parallel for
					for (unsigned i = k; i < size; ++i) {
						lu(i, k - 1) = lu(i, k - 1) / value;//lu(k - 1, k - 1);
					}

					for (unsigned i = k; i < size; ++i) {
						for (unsigned j = k; j < size; ++j) {
							lu(i, j) = lu(i, j) - lu(i, k - 1) * lu(k - 1, j);
						}
					}
				}

				return lu;
			}

		template <typename T>
			vector<T> matrix<T>::solve(const matrix<T> &lu, const vector<T> &b) {
				vector<T> y(b.get_size()), x(b.get_size());

				for (unsigned i = 0; i < b.get_size(); ++i) {
					T tmp = T(0);

#pragma omp parallel for reduction(+:tmp) schedule(static)
					for (unsigned j = 0; j < i; ++j) {
						tmp += lu(i,j) * y[j];
					}

					y[i] = b[i] - tmp;
				}

				for (int i = (int)b.get_size() - 1; i >= 0; --i) {
					T tmp = T(0);

#pragma omp parallel for reduction(+:tmp) schedule(static)
					for (int j = i + 1; j < (int)b.get_size(); ++j) {
						tmp += lu(i,j) * x[j];
					}

					x[i] = (y[i] - tmp) / lu(i,i);
				}

				return x;
			}

		template <typename T>
			inline vector<T> operator *(const vector<T> &v, const matrix<T> &m) {
				//if (m.get_rows() != v.size()) throw std::out_of_range("Matrix dimensions don't match");

				vector<T> result(m.get_cols());

				for (unsigned j = 0; j < m.get_cols(); ++j) {
					T value(0);

#pragma omp parallel for reduction(+:value) schedule(static)
					for (unsigned i = 0; i < m.get_rows(); ++i) {
						value += m(i, j) * v[i];
					}
					result[j] = value;
				}

				return result;
			}
	}
}

template <typename T>
std::ostream &operator <<(std::ostream &os, const util::math::matrix<T> &m) {
	for (unsigned i = 0; i < m.get_rows(); ++i) {
		for (unsigned j = 0; j < m.get_cols(); ++j) {
			os << m(i, j) << " ";
		}
		os << std::endl;
	}

	return os;
}

template <>
std::ostream &operator <<(std::ostream &os, const util::math::matrix<signed char> &m);

#endif //__UTIL_MATH_MATRIX__
