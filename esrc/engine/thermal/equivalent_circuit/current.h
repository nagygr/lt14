#ifndef __ENGINE_THERMAL_EQUIVALENTCIRCUIT_CURRENT__
#define __ENGINE_THERMAL_EQUIVALENTCIRCUIT_CURRENT__

#include <engine/thermal/equivalent_circuit/element.h>

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
			class current : public element {
				public:
					current(unsigned node1, unsigned node2, double value) : element(node1, node2, value) {}

					virtual void add_to(linear_circuit &circuit) override;
					virtual void remove_from(linear_circuit &circuit) override;
					virtual void set_value(double value) override;
			};
		}
	}
}
#endif //__ENGINE_THERMAL_EQUIVALENTCIRCUIT_CURRENT__
