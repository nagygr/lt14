#ifndef __ENGINE_THERMAL_EQUIVALENTCIRCUIT_ELEMENT__
#define __ENGINE_THERMAL_EQUIVALENTCIRCUIT_ELEMENT__

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
			class linear_circuit;

			class element {
				protected:
					unsigned node1;
					unsigned node2;
					double value;

				public:
					element(unsigned node1, unsigned node2, double value) : node1(node1), node2(node2), value(value) {}

					virtual void add_to(linear_circuit &circuit) = 0;
					virtual void remove_from(linear_circuit &circuit) = 0;
					virtual void set_value(double value) = 0;
					double get_value() const {return value;}
					unsigned get_node1() const {return node1;}
					unsigned get_node2() const {return node2;}

					void change_value(linear_circuit &circuit, double new_value) {
						remove_from(circuit);
						set_value(new_value);
						add_to(circuit);
					}
			};
		}
	}
}

#endif //__ENGINE_THERMAL_EQUIVALENTCIRCUIT_ELEMENT__ 
