#ifndef __ENGINE_THERMAL_EQUIVALENTCIRCUIT_RESISTOR__
#define __ENGINE_THERMAL_EQUIVALENTCIRCUIT_RESISTOR__

#include <engine/thermal/equivalent_circuit/element.h>

namespace engine {
	namespace thermal {
		namespace equivalent_circuit {
			class resistor : public element {
				public:
					resistor(unsigned node1, unsigned node2, double resistance) : element(node1, node2, 1.0 / resistance) {}

					virtual void add_to(linear_circuit &circuit) override;
					virtual void remove_from(linear_circuit &circuit) override;
					virtual void set_value(double resistance) override;
			};
		}
	}
}
#endif //__ENGINE_THERMAL_EQUIVALENTCIRCUIT_RESISTOR__
