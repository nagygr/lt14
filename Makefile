.PHONY: all clean docs tarball

all:
	python pct.py all

clean:
	python pct.py clean

docs:
	doxygen Doxyfile

tarball:
	/bin/bash make_tarball.sh lt14
